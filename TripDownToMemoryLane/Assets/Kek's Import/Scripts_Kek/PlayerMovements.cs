﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovements : MonoBehaviour
{
    [Header("Animation")]
    [SerializeField] Animator anim;
    public Transform model;
    [Header("MovementAttributes")]
    [SerializeField] private bool canMove;
    public bool CanMove { get => canMove; set => canMove = value; }
    [SerializeField] private float moveSpeed;
    [SerializeField] private float jumpForwardSpeed;
    [SerializeField] private float jumpHeight;
    [SerializeField] private bool debugActive;
    Vector3 forward;
    Vector3 right;
    bool isGrounded = true;

    [Header("InputHandler")]
    private InputHandler inputHandler;
    float forwardInput;
    float rightInput;

    private Rigidbody rb;
    private float gravity = -9.81f;
    [Header("ForChangeEquipment")]
    public Loadout pLoadout;

    public Animator GetAnimator()
    {
        return anim;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(model.position, .1f);
        Gizmos.DrawWireSphere(model.position + (Vector3.down * .5f), .1f);
    }

    private void Awake()
    {
        CanMove = true;
        forward = Camera.main.transform.forward;
        forward.y = 0;
        forward = Vector3.Normalize(forward);
        right = Quaternion.Euler(new Vector3(0, 90, 0)) * forward;

    }
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        inputHandler = SingletonManager.Get<InputHandler>();
        inputHandler.GetPlayerControls().PlayerControl.Jump.performed += _ => Jump();
        inputHandler.GetPlayerControls().Mouse.DebugPosition.performed += _ => DebugPlayerPosition();
        inputHandler.GetPlayerControls().Equipments.Increase.performed += _ => IncreaseEquipment();
        inputHandler.GetPlayerControls().Equipments.Decrease.performed += _ => DecreaseEquipment();
        inputHandler.GetPlayerControls().Equipments.ResetEquipment.performed += _ => ResetEquipment();
        inputHandler.GetPlayerControls().Equipments.ChangeToShutterSpeed.performed += _ => ChangeEquipment(1);
        inputHandler.GetPlayerControls().Equipments.ChangeToAperture.performed += _ => ChangeEquipment(2);
        inputHandler.GetPlayerControls().Equipments.ChangeToWhiteBalance.performed += _ => ChangeEquipment(3);
        inputHandler.GetPlayerControls().PlayerInteraction.DialogueProceed.performed += _ => ContinueDialogue();

        inputHandler.GetPlayerControls().PlayerInteraction.TestingButton.performed += _ => TestingButtonPerformed();
    }

    // Update is called once per frame
    void Update()
    {
        if (!canMove)
        {
            return;
        }
        forwardInput = inputHandler.GetPlayerControls().PlayerControl.FowardMovement.ReadValue<float>();
        rightInput = inputHandler.GetPlayerControls().PlayerControl.SidewayMovement.ReadValue<float>();
        //Debug.Log("Forward Input: " + forwardInput);
        //Debug.Log("Right Input: " + rightInput);

        Move(forwardInput, rightInput);
        HeightChecker();


        
    }

    private void TestingButtonPerformed()
    {
        //PauseMenu.instance.PauseOrUnpauseGame();
    }

    private void FixedUpdate()
    {
        rb.AddForce(new Vector3(0, 1.0f, 0) * rb.mass * gravity);
    }

    private void ContinueDialogue()
    {
        DialogueSystem.instance.DialogueInput();
    }

    private void DebugPlayerPosition()
    {
        if (!debugActive)
        {
            Debug.Log("Debug is not active");
            return;
        }
        Vector3 mousePosition = inputHandler.GetPlayerControls().Mouse.Position.ReadValue<Vector2>();
        var ray = Camera.main.ScreenPointToRay(mousePosition);

        if(Physics.Raycast(ray, out RaycastHit raycastHit))
        {
            transform.position = raycastHit.point;
        }
    }

    private void ChangeEquipment(int index)
    {
        pLoadout.ChangeEquipment(index);
    }

    public void ResetEquipment()
    {

        pLoadout.ResetCurrentEquipment();
    }

    private void IncreaseEquipment()
    {
        if (canMove)
        {
            pLoadout.UseEquipmentIncreaseInput();
        }
        
    }
    private void DecreaseEquipment()
    {
        if (canMove)
        {
            pLoadout.UseEquipmentDecreaseInput();
        }
        
    }

    private void Move(float forwardInput, float rightInput)
    {
        Vector3 direction = new Vector3(rightInput, 0, forwardInput).normalized;
        Vector3 rightMovement = right * moveSpeed * Time.deltaTime * rightInput;
        Vector3 upMovement = forward * moveSpeed * Time.deltaTime * forwardInput;

        Vector3 heading = Vector3.Normalize(rightMovement + upMovement);
        if (model != null)
        {
            if (direction.magnitude >= 0.1f)
            {
                model.transform.forward = heading;
            }
        }
        else
        {
            transform.forward = heading;
        }
        
        transform.position += rightMovement;
        transform.position += upMovement;

        //transform.GetComponent<Rigidbody>().velocity += rightMovement;
        //transform.GetComponent<Rigidbody>().velocity += upMovement;
        PlayerAnimation(forwardInput, rightInput);

    }

    private void PlayerAnimation(float forwardInput, float rightInput)
    {
        if (forwardInput != 0 || rightInput != 0)
        {
            anim.SetFloat("Speed", 1);
        }
        else
        {
            anim.SetFloat("Speed", 0);
        }

        anim.SetBool("Falling", !IsGrounded() );
        
    }

    public void SetCantMove()
    {
        canMove = false;
        anim.SetFloat("Speed", 0);
    }

    public void BumpedToHazard()
    {
        StopCoroutine("RemoveForce");
        StartCoroutine("RemoveForce");
    }

    private void Jump()
    {
        if (Time.timeScale !=0&&canMove)
        {
            Vector3 jumpVelocity = (model.forward * jumpForwardSpeed);
            Vector3 jumpHeightVelocity = model.up * jumpHeight;
            //Debug.Log("Jump Velocity: " + jumpVelocity);
            if (IsGrounded())
            {
                SoundManager.PlaySound(SoundManager.Sound.PlayerJump, 0.2f);
                //gameObject.GetComponent<Rigidbody>().velocity += jumpVelocity;
                //gameObject.GetComponent<Rigidbody>().velocity += jumpHeightVelocity;
                rb.velocity = Vector3.up * jumpHeight;
                anim.SetTrigger("Jump");

            }

            if (GetComponent<CraneInteract>())
            {
                if (GetComponent<CraneInteract>().isAttached)
                {
                    anim.SetBool("IsAttach", false);
                    GetComponent<CraneInteract>().DetachToCrane();
                    gameObject.GetComponent<Rigidbody>().velocity += jumpVelocity;
                    gameObject.GetComponent<Rigidbody>().velocity += jumpHeightVelocity;
                }
            }
        }
    }

    private bool IsGrounded()
    {
        bool isGrounded = false;
        Collider[] overlap = Physics.OverlapCapsule(model.position,model.position+(Vector3.down*.1f),.3f);
        CapsuleCollider boxCollider = GetComponent<CapsuleCollider>();
        float extraHeightText = 0.01f;

        RaycastHit hit;
        Color rayColor;

        if (Physics.Raycast(boxCollider.bounds.center, Vector3.down, out hit, boxCollider.bounds.extents.y + extraHeightText))
        {
            if(hit.collider != null)
            {
                rayColor = Color.green;
            }
            else
            {
                rayColor = Color.red;
            }
        }
        if (overlap.Length > 0)
        {
            foreach (Collider col in overlap)
            {
                if(col.gameObject != this.gameObject)
                {
                    if (col.isTrigger)
                    {
                        isGrounded = false;
                    }
                    else
                    {
                        isGrounded = true;
                        break;
                    }
                }
                else
                {
                    isGrounded = false;
                }
                
            }
        }
        else
        {
            isGrounded = false;
        }

        Debug.DrawRay(boxCollider.bounds.center, Vector3.down * (boxCollider.bounds.extents.y + extraHeightText));
        //Debug.Log("IsGrounded = " + isGrouded);
        //return hit.collider!= null;
        return isGrounded;
    }

    private void HeightChecker()
    {
        if(GuidedJump.Instance == null) { return; }
        if (IsGrounded())
        {
            GuidedJump.Instance.GuideOff();
        }
       


    }



    IEnumerator RemoveForce()
    {
        anim.SetTrigger("Hurt");
        yield return new WaitForSeconds(.25f);
        rb.velocity = Vector3.zero;
    }
}
