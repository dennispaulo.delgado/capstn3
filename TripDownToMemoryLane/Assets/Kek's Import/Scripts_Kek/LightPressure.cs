using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class OnLightPressureHit : UnityEvent
{

}

[System.Serializable]
public class OnLightPressureExit : UnityEvent
{

}


public class LightPressure : MonoBehaviour, IFeedback
{
    public LightPressure bindWithLightPressure;

    public UnityEvent _onCompletePuzzle;
    public OnLightPressureHit onLightPressureHit;
    public OnLightPressureExit onLightPressureExit;

    public Material glowMat;
    public GameObject onCompleteVFX;
    public bool IsComplete { get { return isComplete; }  }
    public bool usesIntensity;
    private int intensityMeterTaken;

    

    private bool isHit;
    [SerializeField] bool isComplete;
    private float maxCompletion = 100.0f;
    [SerializeField] private float completionAmount = 500.0f;

    [Header("White Balance")]
    public bool isAffectedByWB = false;
    public ColorEnum colorCode;
    private bool isColorCoded = false;

    // Start is called before the first frame update
    void Start()
    {
        if (isAffectedByWB)
        {
            EquipmentChangesEvents.instance.changeWhiteBalance += CheckColorCode;
        }
        glowMat.SetFloat("_RimPower", 5);

    }

    // Update is called once per frame
    void Update()
    {
        if (isComplete)
        {
            glowMat.SetFloat("_RimPower", 0);
            return;
        }

        if (!isAffectedByWB)
        {
            LightPressureHit();
        }
        else
        {
            if (isColorCoded && isHit)
            {
                LightPressureHit();
            }
            else
            {
                if (completionAmount <= 500f)
                {
                    completionAmount += 5f;
                }
                glowMat.SetFloat("_RimPower", completionAmount / maxCompletion);
                isComplete = false;
            }
        }

    }

    private void LightPressureHit()
    {
        if (isHit)
        {
            if (completionAmount >= 0f)
            {
                completionAmount -= 5f;
            }


            glowMat.SetFloat("_RimPower", completionAmount / maxCompletion);
            if (glowMat.GetFloat("_RimPower") <= 0 && isComplete == false)
            {
                isComplete = true;

                if(usesIntensity)
                    GetComponent<IntensitySphere>().onLightIntensityHit?.Invoke(intensityMeterTaken);

                _onCompletePuzzle?.Invoke();
                Instantiate(onCompleteVFX, transform.position + new Vector3(0, 1, 0), Quaternion.identity);
                SoundManager.PlaySound(SoundManager.Sound.PuzzleComplete, 0.2f);
            }


        }
        else
        {
            if (completionAmount <= 500f)
            {
                completionAmount += 5f;
            }

            glowMat.SetFloat("_RimPower", completionAmount / maxCompletion);
            isComplete = false;
        }
    }

    private void CheckColorCode(ColorEnum colNum)
    {
        if (colorCode == colNum)
        {
            isColorCoded = true;
        }
        else
        {
            isColorCoded = false;
        }
    }
    public bool GetIsHit()
    {
        return isHit;
    }

    public void IsHit()
    {
        if (!isComplete)
        {
            isHit = true;
        }
        
    }

    public void IsExit()
    {
        isHit = false;
    }

    public void PowerOn(int id)
    {
        GameEvent.current.OnPowerActive(id);
    }

    public void SetIntensityMeterTaken(int value)
    {
        intensityMeterTaken = value;
    }

    public void OnComplete()
    {
        
    }

    public void OnDeactivate()
    {
        
    }
}
