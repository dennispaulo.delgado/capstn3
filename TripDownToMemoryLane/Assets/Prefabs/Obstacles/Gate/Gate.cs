using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Gate : MonoBehaviour
{
    public Material matLightOn;
    public List<GameObject> lightFeedback;
    public bool needKey;
    public bool hasPower = true;
    public int idToUnlock;
    public int idToPower;
    public List<Animator> anim;
    void Start()
    {
        GameEvent.current.onGateOpen += LightFeedback;
        GameEvent.current.onPowerActive += PowerOn;
        GameEvent.current.onPuzzleSlidingComplete += LightFeedback;
        GameEvent.current.onPuzzleSlidingComplete += PowerOn;
        
    }
    private void CheckPlayerHaveKey(Inventory inv)
    {
        if(inv.keyInventory.Count<=0)
        {
            foreach(Animator a in anim)
            {
                a.SetBool("isOpen", false);
            }
        }
        else
        {
            for(int i = 0; i< inv.keyInventory.Count;i++)
            {
                if(idToUnlock==inv.keyInventory[i].keyID)
                {
                    foreach (Animator a in anim)
                    {
                        a.SetBool("isOpen", true);
                    }
                    GameEvent.current.OnGateOpen(idToUnlock);
                    return;
                }
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!hasPower)
        {
            return;
        }

        if(other.gameObject.tag == "Player")
        {
            if(needKey)
            {
                CheckPlayerHaveKey(other.GetComponent<Inventory>());
            }
            else
            {
                foreach (Animator a in anim)
                {
                    Debug.Log("Gate Is Opne");
                    a.SetBool("isOpen", true);
                }
                if (lightFeedback.Count > 0)
                {
                    foreach (var obj in lightFeedback)
                    {
                        obj.GetComponent<MeshRenderer>().material = matLightOn;
                    }
                }
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (!hasPower)
        {
            return;
        }

        foreach (Animator a in anim)
        {
            a.SetBool("isOpen", false);
        }
    }


    public void LightFeedback(int id)
    {
        if (id == idToUnlock)
        {
            if (lightFeedback.Count > 0)
            {
                foreach (var obj in lightFeedback)
                {
                    obj.GetComponent<MeshRenderer>().material = matLightOn;
                }
            }
            
        }
            
    }

   

    private void PowerOn(int id)
    {
        if(id == idToPower)
        {
            Debug.Log("On");
            hasPower = true;
        }
       
        
    }

    private void PowerDown()
    {
        hasPower = false;
    }

    public void ForcePowerOn()
    {
        hasPower = true;
    }
}
