using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RayLightIntensityHUD : MonoBehaviour
{
    public static RayLightIntensityHUD instance;
    public GameObject intensityTemplate;
    public List<GameObject> intensityList;
    public Image image;
    public Sprite withoutIntensity;
    public Sprite withIntensity;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(this.gameObject);
        }

    }

    public void ClearIndicators()
    {
        if (intensityList.Count > 0)
        {
            for (int i = 0; i < intensityList.Count; i++)
            {
                Destroy(intensityList[i]);
            }
            intensityList.Clear();
        }

    }
    
    public void CheckIfIntensity(bool withOrWithout)
    {
        switch(withOrWithout)
        {
            case true:
                Debug.Log("Has Intensity");
                image.sprite = withIntensity;
                break;
            case false:
                Debug.Log("Has no Intensity");
                image.sprite = withoutIntensity;
                break;
        }
    }

    public void AddRayLightIndicatorsToHUD(Transform rayLightContainer, Transform curUseRL)
    {
        foreach(Transform rayLght in rayLightContainer)
        {
            if (rayLght.GetComponent<IntensityControl>() != null)
            {
                GameObject rayL = Instantiate(intensityTemplate, this.transform);
                if(rayLght == curUseRL)
                {
                    rayL.GetComponent<RaylightIntensityIndicator>().SetReferences(rayLght, this.gameObject.transform.parent.GetComponent<RectTransform>().parent.GetComponent<RectTransform>(), rayLght.GetComponent<IntensityControl>(),true);
                }
                else
                {
                    rayL.GetComponent<RaylightIntensityIndicator>().SetReferences(rayLght, this.gameObject.transform.parent.GetComponent<RectTransform>().parent.GetComponent<RectTransform>(), rayLght.GetComponent<IntensityControl>(),false);
                }
                intensityList.Add(rayL);
            }
        }
    }

    public List<GameObject> ReturnIndicatorList()
    {
        return intensityList;
    }


    public void Hide()
    {
        gameObject.SetActive(false);
    }
}
