using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ObjectiveSlot/Slot")]
public class ObjectiveSlot : ScriptableObject
{
    [Header("For All ObjectiveTypes")]
    [Tooltip("Need for Goal checking")] public int objID;
    [TextArea(5, 20)]
    [Tooltip("Will sho up in UI")] public string objDescription;
    //public List<SlotGoal> goals;
    [Space]
    public SlotGoal goal;
    [Header("For Chained Quest")]
    public ObjectiveSlot chainedObjective;
}

