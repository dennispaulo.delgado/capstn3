using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public Image buttonDescription;

    public GameObject buttons;

    public GameObject continueButton;
    private void Start()
    {
        Time.timeScale = 1;
        if(SaveManager.instance.hasLoaded)
        {
            continueButton.SetActive(true);
        }
        else
        {
            continueButton.SetActive(false);
        }
    }
    public void ExitGame()
    {
        Application.Quit();
    }

    public void ContinueGame()
    {
        if (SaveManager.instance.hasLoaded)
        {
            int lvlIndex = SaveManager.instance.activeSave.currentLvl;

            SceneManager.LoadScene(lvlIndex);
        }
    }

    public void NewGame()
    {
        SaveManager.instance.DeleteSaveData();
        SceneManager.LoadScene(1);
    }

    public void DeleteData()
    {
        SaveManager.instance.DeleteSaveData();
    }

    public void ChangeButtonDescription(Sprite dscrptn)
    {
        buttonDescription.gameObject.SetActive(true);
        buttonDescription.sprite = dscrptn;
        buttonDescription.GetComponent<RectTransform>().sizeDelta = new Vector2(dscrptn.rect.width, dscrptn.rect.height);
    }

    public void UnSelectButtons()
    {
        foreach(Transform btt in buttons.transform)
        {
            if(btt.GetComponent<MainMenuButton>()!=null)
            {
                btt.GetComponent<MainMenuButton>().OnUnHoverMouse();
            }
        }
    }

    public void ResetButtonDescription()
    {
        buttonDescription.gameObject.SetActive(false);
    }
}
