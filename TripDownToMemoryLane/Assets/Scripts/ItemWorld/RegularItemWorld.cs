using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RegularItemWorld : ItemWorld
{
    public OnKeyPickUp onKeyPickUp;

    public int regItemID;
    [Header("ForRegularItems")]
    public RegularItemSO item;
    public RegularItem itemToGive;
    public GameObject vfx;
    void Start()
    {
        MakeItem();
    }
    private void MakeItem()
    {
        itemToGive = new RegularItem();
        itemToGive.name = item.itemName;
        itemToGive.description = item.itemDescription;
        itemToGive.type = item.itemType;
        itemToGive.amount = amount;
    }
    public RegularItem ReturnRegularItem()
    {
        ObjectiveEvents.instance.OnCollectObjective(regItemID);
        return itemToGive;
    }
    private void OnDestroy()
    {
        onKeyPickUp?.Invoke();
    }
}
