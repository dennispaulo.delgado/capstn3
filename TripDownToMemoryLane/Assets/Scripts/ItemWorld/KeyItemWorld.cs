using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class OnKeyPickUp : UnityEvent
{

}


public class KeyItemWorld : ItemWorld
{
    public OnKeyPickUp onKeyPickUp;

    public int keyID;
    [Header("ForKeyItems")]
    public KeyItemSO keyItem;
    public KeyItem itemKeyToGive;
    public GameObject vfx;
    void Start()
    {
        MakeItem();
    }
    private void MakeItem()
    {
        itemKeyToGive = new KeyItem();
        itemKeyToGive.name = keyItem.itemName;
        itemKeyToGive.description = keyItem.itemDescription;
        itemKeyToGive.type = keyItem.itemType;
        itemKeyToGive.amount = amount;
        itemKeyToGive.keyID = keyItem.itemKeyID;
    }
    public KeyItem ReturnKeyItem()
    {
        ObjectiveEvents.instance.OnCollectObjective(keyID);
        return itemKeyToGive;
    }

    private void OnDestroy()
    {
        onKeyPickUp?.Invoke();
    }
}
