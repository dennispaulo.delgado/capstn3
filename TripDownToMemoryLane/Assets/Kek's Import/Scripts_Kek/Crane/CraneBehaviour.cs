using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CraneBehaviour : MonoBehaviour
{
    public CraneMovement movementVariables;
    public CraneMovement.Type movementType;
    float interactableLength = 4f;
   
    public Transform objectToMove;
    public Transform parentObject;
    public Rigidbody objectToConnect;

    private CraneMovementWaypoints waypoints;
    private float timeScaleMultiplier = 1;
    private Vector3 originalPosition;





    // Start is called before the first frame update
    void Start()
    {
        if(movementType == CraneMovement.Type.Horizontal)
        {
            waypoints = GetComponent<CraneMovementWaypoints>();
        }
        originalPosition = objectToMove.transform.position;
        EquipmentChangesEvents.instance.changeTimeScale += ChangeSpeedBaseOnTimeScale;
    }

    // Update is called once per frame
    void Update()
    {
        switch (movementType)
        {
            case CraneMovement.Type.Rotation:
                {
                    objectToMove.Rotate(0, movementVariables.rotationSpeed * Time.deltaTime * timeScaleMultiplier, 0);
                    break;
                }

            case CraneMovement.Type.Horizontal:
                {
                    objectToMove.transform.Translate(Vector3.right * Time.deltaTime * movementVariables.GetMoveDirection() * movementVariables.horizontalSpeed * timeScaleMultiplier);
                    CheckPlatformDistance();
                    break;
                }

            case CraneMovement.Type.HorizontalV2:
                {
                    objectToMove.transform.Translate(Vector3.forward * Time.deltaTime * movementVariables.GetMoveDirection()* movementVariables.horizontalSpeed * timeScaleMultiplier);
                    CheckPlatformDistance();
                    break;
                }
                

            case CraneMovement.Type.Vertical:
                {
                    break;
                }
        }


        Collider[] player = Physics.OverlapSphere(objectToConnect.position, interactableLength);

        foreach (var i in player)
        {
            if (i.gameObject.GetComponent<CraneInteract>())
            {
                i.gameObject.GetComponent<CraneInteract>().craneNearby = true;
                i.gameObject.GetComponent<CraneInteract>().SetObjectToConnect(objectToConnect);
                i.gameObject.GetComponent<CraneInteract>().AttachToCrane();
                //AttachToCrane(i);

            }


        }

    }

    private void AttachToCrane(Collider i)
    {
        i.transform.parent = parentObject;

        if (!i.gameObject.GetComponent<ConfigurableJoint>())
        {
            
            i.gameObject.transform.rotation = Quaternion.Euler(0, 0, 0);
            i.gameObject.AddComponent<ConfigurableJoint>();
            i.gameObject.GetComponent<ConfigurableJoint>().xMotion = ConfigurableJointMotion.Locked;
            i.gameObject.GetComponent<ConfigurableJoint>().yMotion = ConfigurableJointMotion.Locked;
            i.gameObject.GetComponent<ConfigurableJoint>().zMotion = ConfigurableJointMotion.Locked;
            i.gameObject.GetComponent<ConfigurableJoint>().autoConfigureConnectedAnchor = false;
            i.gameObject.GetComponent<ConfigurableJoint>().connectedBody = objectToConnect;
            i.gameObject.GetComponent<ConfigurableJoint>().connectedAnchor = new Vector3(0, -1.5f, 0);

        }
    }

    private void CheckPlatformDistance()
    {
        switch (movementType)
        {
            case CraneMovement.Type.Horizontal:
                if (objectToMove.transform.position.x >= originalPosition.x + (movementVariables.distanceTravel / 2))
                {
                    movementVariables.SetMoveDirection(-1);
                }
                else if (objectToMove.transform.position.x <= originalPosition.x - (movementVariables.distanceTravel / 2))
                {
                    movementVariables.SetMoveDirection(1);
                }
                break;
            case CraneMovement.Type.Vertical:
                if (objectToMove.transform.position.y >= originalPosition.y + (movementVariables.distanceTravel / 2))
                {
                    movementVariables.SetMoveDirection(-1);
                }
                else if (objectToMove.transform.position.y <= originalPosition.y - (movementVariables.distanceTravel / 2))
                {
                    movementVariables.SetMoveDirection(1);
                }
                break;

            case CraneMovement.Type.HorizontalV2:
                if (objectToMove.transform.position.z >= originalPosition.z + (movementVariables.distanceTravel / 2))
                {
                    movementVariables.SetMoveDirection(-1);
                }
                else if (objectToMove.transform.position.z <= originalPosition.z - (movementVariables.distanceTravel / 2))
                {
                    movementVariables.SetMoveDirection(1);
                }
                break;

        }
    }




    private void ChangeSpeedBaseOnTimeScale(float timeScale)
    {
        timeScaleMultiplier = timeScale;
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(objectToConnect.position, interactableLength);
    }



}
