using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CraneMovement : MonoBehaviour
{
    public enum Type
    {
        Horizontal,
        HorizontalV2,
        Vertical,
        Rotation
    };

    public float horizontalSpeed = 20f;
    public float verticalSpeed = 20f;
    public float rotationSpeed = 20f;
    public float distanceTravel;

    [SerializeField][Range(-1, 1)] private int moveDirection = 1;

    public int GetMoveDirection()
    {
        return moveDirection;
    }

    public void SetMoveDirection(int value)
    {
        moveDirection = value;
    }
}
