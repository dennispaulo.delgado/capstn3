﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectorPlatform : MonoBehaviour
{
    public float platformSpeed = 5f;
    public float timeNeeded = 1f;
    public Transform waypoint;

    int num;

    // Start is called before the first frame update
    void Start()
    {

        MovePlatform();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void MovePlatform()
    {


        LeanTween.moveZ(gameObject, waypoint.position.z,timeNeeded).setLoopPingPong();

       

    }

   
}
