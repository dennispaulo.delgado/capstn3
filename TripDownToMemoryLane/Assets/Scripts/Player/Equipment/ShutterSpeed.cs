using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="Equipment/ShutterSpeed")]
public class ShutterSpeed : Equipment
{

    public List<float> shutterSpeeds;
    private bool resettedEquipment = true;
    [Header("EquipmentFailure")]
    public float ssChangeRate;

    public override void GetReference(Loadout refer)
    {
        pLoadout = refer;
    }

    public override void OnDecrease()
    {
        if (canUseEquipment)
        {
            ChangeShutterSpeedIndex(-1);
            PlayerHUD.instance.OnSliderClick(equipmentType,AbilitySlider.Decrease);
        }
    }

    public override void OnEquipped()
    {
        //currentIndexes = pLoadout.ReturnAbilityIndex(PlayerAbility.ShutterSpeed);
        PlayerHUD.instance.MakeLogoActive(PlayerAbility.ShutterSpeed);
        PlayerHUD.instance.ChangeSliderDisplay(equipmentType, (shutterSpeeds[currentIndexes] + "X").ToString());
    }

    public override void OnIncrease()
    {
        if (canUseEquipment)
        {
            ChangeShutterSpeedIndex(1);
            PlayerHUD.instance.OnSliderClick(equipmentType, AbilitySlider.Increase);
        }
    }

    public override void OnUnEquipped()
    {
    }

    public override void OnUse()
    {
    }

    private void PlayShutterSpeed()
    {
        SoundManager.PlaySound(SoundManager.Sound.ShutterSpeed, 0.7f);
    }



    private void ChangeShutterSpeedIndex(int i)
    {
        if (shutterSpeeds.Count <= 0)
        {
            return;
        }
        else if (shutterSpeeds.Count == 1)
        {
            currentIndexes = 0;
        }
        else if (shutterSpeeds.Count > 1)
        {
            if (currentIndexes + i < 0)
            {
                currentIndexes = 0;
            }
            else if (currentIndexes + i > shutterSpeeds.Count - 1)
            {
                currentIndexes = shutterSpeeds.Count - 1;
            }
            else
            {
                currentIndexes += i;
            }
        }
        ChangeShutterSpeed(currentIndexes);
    }
    
    private void ChangeShutterSpeed(int index)
    {
        PlayShutterSpeed();
        TimeManager.instance.timeMultiplier = shutterSpeeds[index];
        EquipmentChangesEvents.instance.ChangeTimeScale(shutterSpeeds[index]);
        PlayerHUD.instance.ChangeSliderDisplay(equipmentType, (shutterSpeeds[index] + "X").ToString());
        if (index > 4)
        {
            GlobalVolumeManager.instance.ActivateShutterSpeedEffect();
        }
        else
        {

            GlobalVolumeManager.instance.DeactivateShutterSpeedEffect();
        }

        if(index > 3)
        {
            var trailContainer = FindObjectOfType<PlayerTrails>();
            var playerTrails = FindObjectOfType<PlayerTrails>().ghostTrail;

            if (!trailContainer.isActive) { return; }
            else
            {
                foreach (var trails in playerTrails)
                {
                    trails.gameObject.SetActive(false);
                }
                trailContainer.isActive = false;
            }
        }
        else
        {
            var trailContainer = FindObjectOfType<PlayerTrails>();
            var playerTrails = FindObjectOfType<PlayerTrails>().ghostTrail;

            if (trailContainer.isActive) { return; }
            else
            {
                foreach (var trails in playerTrails)
                {
                    trails.gameObject.SetActive(true);
                    trails.StartGhostTrail();
                }
                trailContainer.isActive = true;
            }
        }

        //pLoadout.SetAbilityIndex(PlayerAbility.ShutterSpeed, index);
    }

    public override void ResetEquipment()
    {
        currentIndexes = defaultIndex;
        TimeManager.instance.timeMultiplier = shutterSpeeds[currentIndexes];
        if(pLoadout.currentEquipment.equipmentType==equipmentType)
        {
            PlayerHUD.instance.ChangeSliderDisplay(equipmentType, (shutterSpeeds[currentIndexes] + "X").ToString());
        }
        EquipmentChangesEvents.instance.ChangeTimeScale(shutterSpeeds[currentIndexes]);
        if (currentIndexes != defaultIndex)
        {
            GlobalVolumeManager.instance.ActivateShutterSpeedEffect();
        }
        else
        {
            GlobalVolumeManager.instance.DeactivateShutterSpeedEffect();
        }

        var trailContainer = FindObjectOfType<PlayerTrails>();
        var playerTrails = FindObjectOfType<PlayerTrails>().ghostTrail;

        if (!trailContainer.isActive) { return; }
        else
        {
            foreach (var trails in playerTrails)
            {
                trails.gameObject.SetActive(false);
            }
            trailContainer.isActive = false;
        }

        //pLoadout.SetAbilityIndex(PlayerAbility.ShutterSpeed, defaultIndex);
    }

    public override void EquipmentCooldown()
    {
        PlayerHUD.instance.UpdateCooldownHUD(PlayerAbility.ShutterSpeed, curFailCoolDown / defFailCoolDown);
        if (currentIndexes != defaultIndex && canUseEquipment)
        {
            curFailCoolDown -= Time.deltaTime;
            if (curFailCoolDown <= 0)
            {
                canUseEquipment = false;
                resettedEquipment = false;
                TimeManager.instance.ShutterSpeedEquipmentFailureStart(ssChangeRate);
            }
        }
        else
        {
            curFailCoolDown += Time.deltaTime;
            if (curFailCoolDown >= defFailCoolDown)
            {
                curFailCoolDown = defFailCoolDown;
                if (!resettedEquipment)
                {
                    TimeManager.instance.ShutterSpeedEquipmentFailureEnd();
                    ResetEquipment();
                    canUseEquipment = true;
                    resettedEquipment = true;
                }
            }
        }
    }

    public override void ResetToDefault()
    {
        ChangeShutterSpeed(defaultIndex);
    }
}
