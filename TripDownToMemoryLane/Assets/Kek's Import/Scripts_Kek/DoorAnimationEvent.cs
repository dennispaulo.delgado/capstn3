using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorAnimationEvent : MonoBehaviour
{
    public void PlayOpenDoor()
    {
        SoundManager.PlaySound(SoundManager.Sound.DoorOpen, 0.7f);
    }


    public void PlayCloseDoor()
    {
        SoundManager.PlaySound(SoundManager.Sound.DoorClose, 0.7f);
    }
}
