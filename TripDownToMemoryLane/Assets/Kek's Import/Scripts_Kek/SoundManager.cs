using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SoundManager
{
    public enum Sound
    {
        PlayerDamage,
        PlayerJump,
        PropSound,
        DoorOpen,
        DoorClose,
        SlidingPuzzle,
        PuzzleComplete,
        Thunder,
        ShutterSpeed,
        SlidingPuzzleElevate,
    }

    public static void PlaySound(Sound sound, Vector3 position, float volume)
    {
        GameObject soundGameObject = new GameObject("Sound");
        soundGameObject.transform.position = position;
        AudioSource audioSource = soundGameObject.AddComponent<AudioSource>();
        audioSource.volume = volume;
        audioSource.clip = GetAudioClip(sound);
        audioSource.maxDistance = 100f;
        audioSource.spatialBlend = 1f;
        audioSource.rolloffMode = AudioRolloffMode.Linear;
        audioSource.dopplerLevel = 0f;
        audioSource.Play();

        XSectoDestroy.Destroy(soundGameObject, 5f);
    }


    public static void PlaySound(Sound sound, float volume)
    {
        GameObject soundGameObject = new GameObject("Sound");
        AudioSource audioSource = soundGameObject.AddComponent<AudioSource>();
        audioSource.volume = volume;
        audioSource.PlayOneShot(GetAudioClip(sound));

        XSectoDestroy.Destroy(soundGameObject, 5f);
    }

    private static AudioClip GetAudioClip(Sound sound)
    {
        foreach(SoundAssets.SoundAudioClip soundAudioClip in SoundAssets.instance.soundAudioClipArray)
        {
            if(soundAudioClip.sound == sound)
            {
                int rand = Random.Range(0, soundAudioClip.audioClipList.Count);
                return soundAudioClip.audioClipList[rand];
            }

        }
        Debug.LogError($"Sound {sound} not found");
        return null;
    }
}
