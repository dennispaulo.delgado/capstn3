﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using DG.Tweening;

[System.Serializable]
public class OnHitEvent : UnityEvent
{

}
[System.Serializable]
public class OnDisableEvent : UnityEvent
{

}

public class RaylightPuzzle : MonoBehaviour
{
    public OnHitEvent isHit;
    public OnDisableEvent isDisable;

    public float rotationSpeed;
    public Transform firePoint;
    public LayerMask reflectionLayer;
    public bool isActive = false;
    public bool puzzleComplete { get; set; }
        
    private Transform rayLightHead;
    private InteractionHandler interactionHandler;
    private IntensityControl intensityControl;
    [SerializeField] private GameObject objectHit;
   

    private bool canMove = false;

    
    public bool CanMove { get => canMove; set => canMove = value; }


    public GameObject GetObjectHit()
    {
        return objectHit;
    }

    private void Awake()
    {
        rayLightHead = transform.Find("rayLightHead");
        if(GetComponent<IntensityControl>() != null)
        {
            intensityControl = GetComponent<IntensityControl>();
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        interactionHandler = SingletonManager.Get<InteractionHandler>();
        if(intensityControl != null)
        {
            intensityControl.onIntensityChanged += CheckCorrectIntensity;
            intensityControl.onIntensityChanged += CheckIntensityMeter;
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        if (puzzleComplete)
            return;

        if (canMove)
        {
            RotateRayPuzzle();
        }

        if (isActive)
        {
            RaycastHit hit;


            if (Physics.Raycast(firePoint.transform.position, firePoint.transform.forward, out hit, 15f))
            {

                if(hit.collider.CompareTag("RayLight"))
                {

                    objectHit = hit.collider.gameObject.GetComponentInParent<RaylightPuzzle>().gameObject;

                    if (this.isActive == objectHit.GetComponent<RaylightPuzzle>().isActive)
                    {
                        return;
                    }
                    objectHit.GetComponent<RaylightPuzzle>().isHit?.Invoke();
                    if(intensityControl != null)
                        CheckCorrectIntensity();


                }
                //else
                //{
                //    if (objectHit != null)
                //    {
                //        if (objectHit.GetComponent<RaylightPuzzle>())
                //        {
                //            Debug.Log(objectHit.GetComponent<RaylightPuzzle>());
                //            if(this.gameObject == objectHit.GetComponent<RaylightPuzzle>().GetObjectHit())
                //            {
                //                return;
                //            }
                //            objectHit.GetComponent<RaylightPuzzle>().isDisable?.Invoke();
                //            objectHit = null;
                //        }
                       
                //    }
                //}



                if (hit.collider.CompareTag("LightPressure"))
                {

                    objectHit = hit.collider.gameObject;

                    if (objectHit.gameObject.GetComponent<LightPressure>().GetIsHit())
                    {
                        return;
                    }
                    switch (objectHit.gameObject.GetComponent<IntensitySphere>() != null)
                    {
                        case true:
                            {
                                CheckIntensityMeter();
                                break;
                            }

                        case false:
                            {
                                objectHit.gameObject.GetComponent<LightPressure>().onLightPressureHit?.Invoke();
                                break;
                            }
                    }
                    
                   
                    
                    
                }
                else
                {
                    if (objectHit != null)
                    {
                        if (objectHit.GetComponent<LightPressure>())
                        {
                           

                            objectHit.GetComponent<LightPressure>().onLightPressureExit?.Invoke();
                            objectHit = null;
                        }
                    }
                }


            }
            else
            {
                if(objectHit != null)
                {
                    if (objectHit.GetComponent<RaylightPuzzle>())
                    {
                        Debug.Log(objectHit.GetComponent<RaylightPuzzle>());
                        if (this.gameObject == objectHit.GetComponent<RaylightPuzzle>().GetObjectHit())
                        {
                            return;
                        }
                        objectHit.GetComponent<RaylightPuzzle>().isDisable?.Invoke();
                        objectHit = null;
                    }
                }
            }

        }
       

    }

    private void CheckCorrectIntensity()
    {
        if (objectHit == null) return;
        if (objectHit.GetComponent<IntensitySphere>()) return;

        if (this.intensityControl.GetIntensityMeter() == objectHit.GetComponent<IntensityControl>().GetIntensityNeeded())
        {
            objectHit.gameObject.GetComponentInParent<RaylightPuzzle>().isHit?.Invoke();
        }
        else
        {
            objectHit.gameObject.GetComponentInParent<RaylightPuzzle>().isDisable?.Invoke();
        }
    }


    private void CheckIntensityMeter()
    {
        if (objectHit == null) return;
        if (!objectHit.GetComponent<IntensitySphere>()) return;

        var intensityMeter = this.GetComponent<IntensityControl>().GetIntensityMeter();
        var lightPressure = objectHit.gameObject.GetComponent<LightPressure>();
        lightPressure.SetIntensityMeterTaken(intensityMeter);
        lightPressure.onLightPressureHit?.Invoke();

        if (lightPressure.IsComplete)
            objectHit.GetComponent<IntensitySphere>().onLightIntensityHit?.Invoke(intensityMeter);

    }
    

    private void RotateRayPuzzle()
    {
        float rotation = interactionHandler.GetPlayerControls().PlayerInteraction.TurnRayLight.ReadValue<float>();
        float rotate = rotation * rotationSpeed * Time.deltaTime;

        rayLightHead.Rotate(0, rotate, 0);
    }

    public void ActivateLightBeam()
    {
        isActive = true;
    }

    public void DeactivateLightBeam()
    {
        isActive = false;
        if(objectHit != null)
        {
            switch (objectHit.tag)
            {
                case "RayLight":
                    if (this.isActive == objectHit.GetComponent<RaylightPuzzle>().isActive)
                    {
                        return;
                    }
                    objectHit.GetComponent<RaylightPuzzle>().isDisable?.Invoke();
                    objectHit = null;
                    break;

                case "LightPressure":
                    objectHit.GetComponent<LightPressure>().onLightPressureExit?.Invoke();
                    objectHit = null;
                    break;
            }


            
        }
    }





}
