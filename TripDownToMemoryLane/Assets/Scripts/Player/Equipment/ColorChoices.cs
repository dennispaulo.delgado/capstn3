using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ColorChoices
{
    public float temperature;
    public ColorEnum colorEnum;
}

public enum ColorEnum
{
    Regular,
    Cool,
    Warm
}

