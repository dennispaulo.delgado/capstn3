using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectiveUI : MonoBehaviour
{
    public static ObjectiveUI instance;

    public GameObject objTilePrefab;

    public Transform objTilesGroup;

    private void Awake()
    {
        
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(this.gameObject);
        }
       
    }

    private void Start()
    {
        
    }

    public void MakeObjectUITile(Objective obj)
    {
        GameObject tl = Instantiate(objTilePrefab, objTilesGroup);
        tl.SetActive(true);
        tl.GetComponent<ObjectiveUITile>().MakeObjectiveTile(obj);
    }
}
