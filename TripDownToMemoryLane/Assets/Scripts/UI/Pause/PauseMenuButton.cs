using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseMenuButton : MonoBehaviour
{
    public Sprite unselected;
    public Sprite selected;
    public Sprite description;

    public void OnHoverMouse()
    {
        GetComponent<Image>().sprite = selected;
        if (description != null)
        {
            PauseMenu.instance.ChangeButtonDescription(description);
        }
    }
    public void OnUnHoverMouse()
    {
        GetComponent<Image>().sprite = unselected;
        if (description != null)
        {
            PauseMenu.instance.ResetButtonDescription();
        }
    }
}
