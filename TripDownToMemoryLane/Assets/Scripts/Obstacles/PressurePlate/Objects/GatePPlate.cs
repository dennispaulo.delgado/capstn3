using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GatePPlate : PresPlatActivated
{
    public enum GateMove
    {
        Up,
        Down
    }
    public GateMove moveDirection;
    public float dropDistance;
    public float lerpActiveSpeed;
    public float lerpDeactiveSpeed;

    private Vector3 originalPos;
    private void OnDrawGizmos()
    {
        switch (moveDirection)
        {
            case GateMove.Up:
                Gizmos.color = Color.green;
                Vector3 wireCube = new Vector3(this.transform.localScale.x, this.transform.localScale.y, this.transform.localScale.z);
                Gizmos.DrawWireCube(this.transform.position + (this.transform.up * dropDistance), wireCube);
                break;
            case GateMove.Down:
                Gizmos.color = Color.green;
                Vector3 wireCube2 = new Vector3(this.transform.localScale.x, this.transform.localScale.y, this.transform.localScale.z);
                Gizmos.DrawWireCube(this.transform.position + (this.transform.up * dropDistance * -1), wireCube2);
                break;
        }
    }

    private void Start()
    {
        originalPos = this.transform.position;
    }

    public override void Activated()
    {
        switch (moveDirection)
        {
            case GateMove.Up:
                transform.position = new Vector3(transform.position.x, Mathf.Lerp(transform.position.y, originalPos.y + dropDistance, lerpActiveSpeed), transform.position.z);
                break;
            case GateMove.Down:
                transform.position = new Vector3(transform.position.x, Mathf.Lerp(transform.position.y, originalPos.y - dropDistance, lerpActiveSpeed), transform.position.z);
                break;
        }
    }

    public override void Deactivate()
    {
        transform.position = new Vector3(Mathf.Lerp(transform.position.x, originalPos.x, lerpDeactiveSpeed), Mathf.Lerp(transform.position.y, originalPos.y, lerpDeactiveSpeed), Mathf.Lerp(transform.position.z, originalPos.z, lerpDeactiveSpeed));
    }
}
