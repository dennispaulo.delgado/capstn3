using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Activator : MonoBehaviour
{
    public List<Transform> objects;
    private void Start()
    {
        GetChildObjects();
    }

    private void GetChildObjects()
    {
        foreach(Transform obj in this.transform)
        {
            objects.Add(obj);
            obj.gameObject.SetActive(false);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            foreach (Transform obj in objects)
            {
                obj.gameObject.SetActive(true);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            foreach (Transform obj in objects)
            {
                obj.gameObject.SetActive(false);
            }
        }
    }
}
