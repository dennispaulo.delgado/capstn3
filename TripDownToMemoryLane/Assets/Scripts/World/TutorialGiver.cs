using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TutorialGiver : MonoBehaviour
{
    public UnityEvent onTutorialEnd;

    public Sprite tutorialSprite;
    public float tutorialDuration;
    public bool isActive = true;

    public void ActivateTutorial()
    {
        TutorialUI.instance.DisplayTutorial(tutorialSprite, tutorialDuration);
        TutorialUI.instance.OnFinishTutorialShow.AddListener(OnFinishTutorialShow);
    }
    private void OnFinishTutorialShow()
    {
        Debug.Log("Tutorial End Call");
        onTutorialEnd.Invoke();
        TutorialUI.instance.OnFinishTutorialShow.RemoveListener(OnFinishTutorialShow);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            if(isActive)
            {
                ActivateTutorial();
                isActive = false;
            }
        }
    }
}
