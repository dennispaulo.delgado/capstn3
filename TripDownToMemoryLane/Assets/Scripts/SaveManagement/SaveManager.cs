using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

public class SaveManager : MonoBehaviour
{
    public static SaveManager instance;

    public SaveData activeSave;

    public bool hasLoaded;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            Load();
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Save()
    {
        string datapath = Application.persistentDataPath;

        var serializer = new XmlSerializer(typeof(SaveData));
        var stream = new FileStream(datapath + "/" + activeSave.saveName + ".save" , FileMode.Create);
        serializer.Serialize(stream, activeSave);
        stream.Close();

        Debug.Log("Saved");
    }

    public void Load()
    {
        string datapath = Application.persistentDataPath;

        if(System.IO.File.Exists(datapath + "/" + activeSave.saveName + ".save"))
        {
            var serializer = new XmlSerializer(typeof(SaveData));
            var stream = new FileStream(datapath + "/" + activeSave.saveName + ".save", FileMode.Open);
            activeSave = serializer.Deserialize(stream) as SaveData;
            stream.Close();
            hasLoaded = true;
            Debug.Log("Loaded");
        }

    }

    public void DeleteSaveData()
    {
        string datapath = Application.persistentDataPath;

        if (System.IO.File.Exists(datapath + "/" + activeSave.saveName + ".save"))
        {
            File.Delete(datapath + "/" + activeSave.saveName + ".save");
            Debug.Log("Deleted");
        }
    }
}

[System.Serializable]
public class SaveData
{
    public string saveName;
    public int currentLvl;
}