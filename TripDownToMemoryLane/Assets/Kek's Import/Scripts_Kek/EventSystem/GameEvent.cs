﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEvent : MonoBehaviour
{
    public static GameEvent current;

    private void Awake()
    {
        current = this;
    }

    public event Action<int> onProjectorObjectTriggerEnter;
    public void ProjectorObjectTriggerEnter(int id)
    {
        onProjectorObjectTriggerEnter?.Invoke(id);
    }

    public event Action<int> onProjectorObjectTriggerExit;
    public void ProjectObjecttriggerExit(int id)
    {
        onProjectorObjectTriggerExit?.Invoke(id);
    }

    public event Action<int> onPuzzleSlidingComplete;
    public void PuzzleSlidingComplete(int id)
    {
        onPuzzleSlidingComplete?.Invoke(id);
    }

    public event Action<int> onPuzzleSlidingCompleteNoMemory;
    public void PuzzleSlidingCompleteNoMemory(int id)
    {
        onPuzzleSlidingCompleteNoMemory?.Invoke(id);
    }

    public event Action onPuzzleSlidingCompleteTransition;
    public void PuzzleSlidingComplete()
    {
        onPuzzleSlidingCompleteTransition?.Invoke();
    }

    public event Action onRayLightInteract;
    public void RayLightInteract()
    {
        onRayLightInteract?.Invoke();
    }

    public event Action onRayLightExit;
    public void RayLightExit()
    {
        onRayLightExit?.Invoke();
    }

    public event Action<int> onGateOpen;
    public void OnGateOpen(int id)
    {
        onGateOpen?.Invoke(id);
    }

    public event Action onCollectedProps;
    public void OnCollectedProps()
    {
        onCollectedProps?.Invoke();
    }

    public event Action onCollectedPropsForDoor;
    public void OnCollectedPropsForDoor()
    {
        onCollectedPropsForDoor?.Invoke();
    }

    public event Action<GameObject, Vector3> onItemPickup;
    public void OnItemPickup(GameObject vfx, Vector3 position)
    {
        onItemPickup?.Invoke(vfx, position);
    }

    public event Action<GameObject, Vector3> onCollectedKey;
    public void OnCollectedKey(GameObject vfx, Vector3 position)
    {
        onCollectedKey?.Invoke(vfx, position);
    }

    public event Action<int> onPowerActive;
    public void OnPowerActive(int id)
    {
        onPowerActive?.Invoke(id);
    }


}
