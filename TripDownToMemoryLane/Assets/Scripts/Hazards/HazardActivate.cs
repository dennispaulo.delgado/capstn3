using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HazardActivate : MonoBehaviour
{
    public bool acivated = false;
    public List<GameObject> hazards;

    public void ActivateHazards()
    {
        foreach(GameObject hzrds in hazards)
        {
            hzrds.SetActive(true);
            acivated = true;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(!acivated)
        {
            ActivateHazards();
        }
    }
}
