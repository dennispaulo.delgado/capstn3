// GENERATED AUTOMATICALLY FROM 'Assets/Scripts/Player/Movement/PlayerMovement.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @PlayerMovement : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @PlayerMovement()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""PlayerMovement"",
    ""maps"": [
        {
            ""name"": ""Land"",
            ""id"": ""18b8086b-1740-46ad-80e2-db340f3fa638"",
            ""actions"": [
                {
                    ""name"": ""Straight"",
                    ""type"": ""PassThrough"",
                    ""id"": ""085829dc-5870-4a6c-8207-866b07968b9a"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Sideway"",
                    ""type"": ""Button"",
                    ""id"": ""dad02386-10f2-4644-aaff-5eaba6e2b701"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Jump"",
                    ""type"": ""Button"",
                    ""id"": ""5933a648-b908-4b23-a968-2529e5d97ec0"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Dash"",
                    ""type"": ""Button"",
                    ""id"": ""5a205dca-63a4-41d2-82e6-bc1409fd8401"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Testing"",
                    ""type"": ""Button"",
                    ""id"": ""147d9da0-51c5-4ee2-ad71-c424143f9051"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""Straight"",
                    ""id"": ""8619506e-2df5-4a6a-814c-bac22486fa98"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Straight"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""8e862084-2bd4-48fe-af37-33955bd04748"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Straight"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""64176407-469b-48ed-bdf1-4f005f16dd84"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Straight"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""2f1896b4-0181-4179-8251-5850f3fc2869"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""Sideway"",
                    ""id"": ""c23bff15-b624-4924-af85-75a5288d8306"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Sideway"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""d3e3f396-dbe7-4657-be52-a52b0767a666"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Sideway"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""7591615e-8abb-4e54-835e-6aca44f7ef20"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Sideway"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""f5a68d93-aacd-4570-a1f6-8f72729db67e"",
                    ""path"": ""<Keyboard>/p"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Testing"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""5709555d-3551-4c5c-9219-f95fe04b50bc"",
                    ""path"": ""<Keyboard>/shift"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Dash"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // Land
        m_Land = asset.FindActionMap("Land", throwIfNotFound: true);
        m_Land_Straight = m_Land.FindAction("Straight", throwIfNotFound: true);
        m_Land_Sideway = m_Land.FindAction("Sideway", throwIfNotFound: true);
        m_Land_Jump = m_Land.FindAction("Jump", throwIfNotFound: true);
        m_Land_Dash = m_Land.FindAction("Dash", throwIfNotFound: true);
        m_Land_Testing = m_Land.FindAction("Testing", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Land
    private readonly InputActionMap m_Land;
    private ILandActions m_LandActionsCallbackInterface;
    private readonly InputAction m_Land_Straight;
    private readonly InputAction m_Land_Sideway;
    private readonly InputAction m_Land_Jump;
    private readonly InputAction m_Land_Dash;
    private readonly InputAction m_Land_Testing;
    public struct LandActions
    {
        private @PlayerMovement m_Wrapper;
        public LandActions(@PlayerMovement wrapper) { m_Wrapper = wrapper; }
        public InputAction @Straight => m_Wrapper.m_Land_Straight;
        public InputAction @Sideway => m_Wrapper.m_Land_Sideway;
        public InputAction @Jump => m_Wrapper.m_Land_Jump;
        public InputAction @Dash => m_Wrapper.m_Land_Dash;
        public InputAction @Testing => m_Wrapper.m_Land_Testing;
        public InputActionMap Get() { return m_Wrapper.m_Land; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(LandActions set) { return set.Get(); }
        public void SetCallbacks(ILandActions instance)
        {
            if (m_Wrapper.m_LandActionsCallbackInterface != null)
            {
                @Straight.started -= m_Wrapper.m_LandActionsCallbackInterface.OnStraight;
                @Straight.performed -= m_Wrapper.m_LandActionsCallbackInterface.OnStraight;
                @Straight.canceled -= m_Wrapper.m_LandActionsCallbackInterface.OnStraight;
                @Sideway.started -= m_Wrapper.m_LandActionsCallbackInterface.OnSideway;
                @Sideway.performed -= m_Wrapper.m_LandActionsCallbackInterface.OnSideway;
                @Sideway.canceled -= m_Wrapper.m_LandActionsCallbackInterface.OnSideway;
                @Jump.started -= m_Wrapper.m_LandActionsCallbackInterface.OnJump;
                @Jump.performed -= m_Wrapper.m_LandActionsCallbackInterface.OnJump;
                @Jump.canceled -= m_Wrapper.m_LandActionsCallbackInterface.OnJump;
                @Dash.started -= m_Wrapper.m_LandActionsCallbackInterface.OnDash;
                @Dash.performed -= m_Wrapper.m_LandActionsCallbackInterface.OnDash;
                @Dash.canceled -= m_Wrapper.m_LandActionsCallbackInterface.OnDash;
                @Testing.started -= m_Wrapper.m_LandActionsCallbackInterface.OnTesting;
                @Testing.performed -= m_Wrapper.m_LandActionsCallbackInterface.OnTesting;
                @Testing.canceled -= m_Wrapper.m_LandActionsCallbackInterface.OnTesting;
            }
            m_Wrapper.m_LandActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Straight.started += instance.OnStraight;
                @Straight.performed += instance.OnStraight;
                @Straight.canceled += instance.OnStraight;
                @Sideway.started += instance.OnSideway;
                @Sideway.performed += instance.OnSideway;
                @Sideway.canceled += instance.OnSideway;
                @Jump.started += instance.OnJump;
                @Jump.performed += instance.OnJump;
                @Jump.canceled += instance.OnJump;
                @Dash.started += instance.OnDash;
                @Dash.performed += instance.OnDash;
                @Dash.canceled += instance.OnDash;
                @Testing.started += instance.OnTesting;
                @Testing.performed += instance.OnTesting;
                @Testing.canceled += instance.OnTesting;
            }
        }
    }
    public LandActions @Land => new LandActions(this);
    public interface ILandActions
    {
        void OnStraight(InputAction.CallbackContext context);
        void OnSideway(InputAction.CallbackContext context);
        void OnJump(InputAction.CallbackContext context);
        void OnDash(InputAction.CallbackContext context);
        void OnTesting(InputAction.CallbackContext context);
    }
}
