using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Equipment : ScriptableObject
{
    public PlayerAbility equipmentType;
    [HideInInspector]public Loadout pLoadout;
    public int currentIndexes;
    public int defaultIndex;
    [Header("EquipmentFailure")]
    public bool canUseEquipment = true;
    public float curFailCoolDown;
    public float defFailCoolDown;

    public abstract void GetReference(Loadout refer);
    public abstract void OnUse();
    public abstract void OnEquipped();
    public abstract void OnUnEquipped();
    public abstract void OnIncrease();
    public abstract void OnDecrease();
    public abstract void ResetEquipment();
    public abstract void EquipmentCooldown();
    public abstract void ResetToDefault();
}
