using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ApertureLights : MonoBehaviour
{
    public AudioClip onLight;
    public AudioClip offLight;
    public float intensityNeeded;
    [Header("Light Reference")]
    public List<Light> apertureLight;
    public List<GameObject> affectedObjs;
    private float radius = 3;
    private float currentSize;
    [Header("Intensity Sprite")]
    public SpriteRenderer sRenderer;
    public List<Sprite> intensitySprites;
    [Header("Events")]
    public UnityEvent onDisappear;

    void Start()
    {
        EquipmentChangesEvents.instance.changeApertureSize += CheckApertureLightStatus;
        GetLightLightReference();

        CheckApertureLightStatus(0);
        ChangeIntensitySprite();
    }

    private void ChangeIntensitySprite()
    {
        switch(intensityNeeded)
        {
            case 0:
                sRenderer.gameObject.SetActive(false);
                break;
            case 1:
                sRenderer.gameObject.SetActive(true);
                sRenderer.sprite = intensitySprites[0];
                break;
            case 2:
                sRenderer.gameObject.SetActive(true);
                sRenderer.sprite = intensitySprites[1];
                break;
            case 3:
                sRenderer.gameObject.SetActive(true);
                sRenderer.sprite = intensitySprites[2];
                break;
            case 4:
                sRenderer.gameObject.SetActive(true);
                sRenderer.sprite = intensitySprites[3];
                break;
            case 5:
                sRenderer.gameObject.SetActive(true);
                sRenderer.sprite = intensitySprites[4];
                break;
        }
    }

    void Update()
    {

    }

    /*
    private void ChangeLightSize(float size)
    {
        apertureLightObject.transform.localScale = new Vector3(size, size, size);
        currentSize = size;
        if (intensityNeeded != 0)
        {
            StartCoroutine("ScaleChangeAnimation", 0);
        }
        else
        {
            StartCoroutine("ScaleChangeAnimation", baseSize);
        }
    }*/
    /*
    private void ChangeApertureLightSize(float scale)
    {
        //ChangeLightSize(scale + baseSize);
        StopCoroutine("ScaleChangeAnimation");
        /*
        if (scale + baseSize <= 0)
        {
            StartCoroutine("ScaleChangeAnimation", 0);
        }
        else
        {
            StartCoroutine("ScaleChangeAnimation", scale + baseSize);
        }*//*
        if (intensityNeeded == scale || intensityNeeded == 0)
        {
            StartCoroutine("ScaleChangeAnimation", baseSize);
        }
        else
        {
            StartCoroutine("ScaleChangeAnimation", 0);
}
    }*/
    private void GetLightLightReference()
    {
        foreach(Transform chld in this.transform)
        {
            if(chld.GetComponent<Light>()!=null)
            {
                apertureLight.Add(chld.GetComponent<Light>());
            }
        }
    }

    private void CheckApertureLightStatus(float intensity)
    {
        if (apertureLight != null)
        {
            if (intensityNeeded == intensity || intensityNeeded == 0)
            {
                foreach (Transform chld in this.transform)
                {
                    if (chld.GetComponent<Light>() != null)
                    {
                        chld.gameObject.SetActive(true);
                        GetComponent<AudioSource>().PlayOneShot(onLight);
                    }
                }
                if (affectedObjs.Count > 0)
                {
                    foreach (GameObject obj in affectedObjs)
                    {
                        obj.SetActive(true);
                        GetComponent<AudioSource>().PlayOneShot(onLight);
                    }
                }
            }
            else
            {
                onDisappear.Invoke();
                foreach (Transform chld in this.transform)
                {
                    if (chld.GetComponent<Light>() != null)
                    {
                        if(chld.gameObject == true)
                            GetComponent<AudioSource>().PlayOneShot(offLight);

                        chld.gameObject.SetActive(false);
                        
                    }
                }

                if (affectedObjs.Count > 0)
                {
                    foreach (GameObject obj in affectedObjs)
                    {
                        if (obj == true)
                            GetComponent<AudioSource>().PlayOneShot(offLight);

                        obj.SetActive(false);
                        
                    }
                }
            }
        }
    }

    /*
    IEnumerator ScaleChangeAnimation(float size)
    {
        yield return new WaitForSeconds(Time.deltaTime);
        if(apertureLightObject.transform.localScale.x != size)
        {
            apertureLightObject.transform.localScale = Vector3.Lerp(apertureLightObject.transform.localScale, new Vector3(size, size, size), 0.01f);
            //float change = animDuration / size;
            //currentSize += change;
            //apertureLightObject.transform.localScale = new Vector3(currentSize, currentSize, currentSize);
            StartCoroutine("ScaleChangeAnimation", size);
        }
        else
        {
        }
    }*/
}
