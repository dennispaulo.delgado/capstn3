using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LightNodes : MonoBehaviour
{
    private Light spotLight;
    [Header("Attritubes")]
    public int projectorID;
    public float targetIntensity;
    public float intensityMultiplier;
    private float defIntensity;

    void Start()
    {
        spotLight = GetComponent<Light>();
        EquipmentChangesEvents.instance.changeApertureSize += IntensityChange;
        defIntensity = spotLight.intensity;
    }

    private void IntensityChange(float amnt)
    {
        spotLight.intensity = defIntensity + (amnt * intensityMultiplier);
        IntensityCheck(amnt);
    }

    private void IntensityCheck(float amnt)
    {
        if (amnt == targetIntensity)
        {
            EquipmentChangesEvents.instance.ChangeLightNode(projectorID, true);

        }
        else
        {
            EquipmentChangesEvents.instance.ChangeLightNode(projectorID, false);
        } 
    }



}
