using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ObjectiveUITile : MonoBehaviour
{
    public Text objectiveDescription;
    public Image objectiveStatus;
    private Objective tileObjective;
    [Header("ObjectiveStatus")]
    public Sprite complete;
    public Sprite incomplete;

    public void MakeObjectiveTile(Objective obj)
    {
        tileObjective = obj;
        switch (tileObjective.goal.goalType)
        {
            case GoalType.collect:
                objectiveDescription.text = tileObjective.goal.currentAmount + "/" + tileObjective.goal.requiredAmount + " " + tileObjective.goal.description;
                objectiveStatus.sprite = incomplete;
                objectiveDescription.color = Color.red;
                break;
            case GoalType.id:
                objectiveDescription.text = tileObjective.goal.description;
                objectiveStatus.sprite = incomplete;
                objectiveDescription.color = Color.red;
                break;
        }
        ObjectiveEvents.instance.evalObjective += UpdateObjectiveTile;
    }

    public void UpdateObjectiveTile()
    {
        switch (tileObjective.goal.goalType)
        {
            case GoalType.collect:
                objectiveDescription.text = tileObjective.goal.currentAmount + "/" + tileObjective.goal.requiredAmount + " " + tileObjective.goal.description;

                if (tileObjective.isCompleted)
                {
                    objectiveStatus.sprite = complete;
                    objectiveDescription.color = Color.green;
                    ObjectiveEvents.instance.evalObjective -= UpdateObjectiveTile;
                }
                break;
            case GoalType.id:
                objectiveDescription.text = tileObjective.goal.description;
                if (tileObjective.isCompleted)
                {
                    objectiveStatus.sprite = complete;
                    objectiveDescription.color = Color.green;
                    ObjectiveEvents.instance.evalObjective -= UpdateObjectiveTile;
                }
                break;
        }
    }
}