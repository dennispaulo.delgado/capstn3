using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class InteractableManager : MonoBehaviour
{
    public static InteractableManager instance;
    public GameObject indicatorTemplate;
    public List<GameObject> indicatorList = new List<GameObject>();
    public List<GameObject> specialIndicatorList = new List<GameObject>();

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(this.gameObject);
        }

    }

    public void AddSpecialIndicator(Transform plyr, Transform objItrctbl)
    {
        if (CheckIfExistingSpecialInteractable(objItrctbl))
        {
            for (int i = 0; i < specialIndicatorList.Count; i++)
            {
                if (specialIndicatorList[i].gameObject.GetComponent<InteractableIndicator>().ReferencedInteractable() == objItrctbl)
                {
                    specialIndicatorList[i].gameObject.GetComponent<InteractableIndicator>().InteractableIsNearest(plyr, objItrctbl);
                    return;
                }
            }
        }
        else
        {
            GameObject intInd = Instantiate(indicatorTemplate, this.transform);
            intInd.GetComponent<InteractableIndicator>().SetReferences(plyr, objItrctbl, this.gameObject.transform.parent.GetComponent<RectTransform>(), this);
            specialIndicatorList.Add(intInd);
        }
    }
    public void RemoveSpecialIndicator(Transform objItrctbl)
    {
        if (CheckIfExistingSpecialInteractable(objItrctbl))
        {
            for (int i = 0; i < specialIndicatorList.Count; i++)
            {
                if (specialIndicatorList[i].gameObject.GetComponent<InteractableIndicator>().ReferencedInteractable() == objItrctbl)
                {
                    specialIndicatorList[i].gameObject.GetComponent<InteractableIndicator>().RemoveIndicator();
                    return;
                }
            }
        }
    }
    public void ClearSpecialIndicators()
    {
        if (specialIndicatorList.Count > 0)
        {
            for (int i = 0; i < specialIndicatorList.Count; i++)
            {
                Destroy(specialIndicatorList[i]);
            }
            specialIndicatorList.Clear();
        }
    }
    public void ClearIndicators()
    {
        if (indicatorList.Count > 0)
        {
            for (int i = 0; i < indicatorList.Count; i++)
            {
                Destroy(indicatorList[i]);
            }
            indicatorList.Clear();
        }
    }
    public void IndicatorIsNearest(Transform plyr, Transform objItrctbl)
    {
        if (CheckIfExistingInteractable(objItrctbl))
        {
            for (int i = 0; i < indicatorList.Count; i++)
            {
                if (indicatorList[i].gameObject.GetComponent<InteractableIndicator>().ReferencedInteractable() == objItrctbl)
                {
                    indicatorList[i].gameObject.GetComponent<InteractableIndicator>().InteractableIsNearest(plyr, objItrctbl);
                    return;
                }
            }
        }
        else
        {
            GameObject intInd = Instantiate(indicatorTemplate, this.transform);
            intInd.GetComponent<InteractableIndicator>().SetReferences(plyr, objItrctbl, this.gameObject.transform.parent.GetComponent<RectTransform>(), this);
            indicatorList.Add(intInd);
        }
    }

    public void RemoveIndicator(Transform objItrctbl)
    {
        if (CheckIfExistingInteractable(objItrctbl))
        {
            for (int i = 0; i < indicatorList.Count; i++)
            {
                if (indicatorList[i].gameObject.GetComponent<InteractableIndicator>().ReferencedInteractable() == objItrctbl)
                {
                    indicatorList[i].gameObject.GetComponent<InteractableIndicator>().RemoveIndicator();
                    return;
                }
            }
        }
    }

    public void IndicatorInRange(Transform plyr, Transform objItrctbl)
    {

        if (CheckIfExistingInteractable(objItrctbl))
        {
            for (int i = 0; i < indicatorList.Count; i++)
            {
                if (indicatorList[i].gameObject.GetComponent<InteractableIndicator>().ReferencedInteractable() == objItrctbl)
                {
                    indicatorList[i].gameObject.GetComponent<InteractableIndicator>().InteractableInRange(plyr, objItrctbl);
                    return;
                }
            }
        }
        else
        {
            GameObject intInd = Instantiate(indicatorTemplate, this.transform);
            intInd.GetComponent<InteractableIndicator>().SetReferences(plyr, objItrctbl, this.gameObject.transform.parent.GetComponent<RectTransform>(), this);
            indicatorList.Add(intInd);
        }
    }

    public bool CheckIfExistingInteractable(Transform objItrctbl)
    {
        for (int i = 0; i < indicatorList.Count; i++)
        {
            if (indicatorList[i].gameObject.GetComponent<InteractableIndicator>().ReferencedInteractable() == objItrctbl)
            {
                return true;
            }
        }
        return false;
    }
    public bool CheckIfExistingSpecialInteractable(Transform objItrctbl)
    {
        for (int i = 0; i < indicatorList.Count; i++)
        {
            if (indicatorList[i].gameObject.GetComponent<InteractableIndicator>().ReferencedInteractable() == objItrctbl)
            {
                return true;
            }
        }
        return false;
    }

    public void RemoveIndicatorInList(GameObject indi)
    {
        for (int i = 0; i < indicatorList.Count; i++)
        {
            if (indi == indicatorList[i].gameObject)
            {
                indicatorList.RemoveAt(i);
                Destroy(indi);
                return;
            }
        }
    }

    public List<GameObject> ReturnIndicatorList()
    {
        return indicatorList;
    }
    public void UnHide()
    {
        foreach (GameObject ind in indicatorList)
        {
            ind.SetActive(true);
        }
        foreach (GameObject sInd in specialIndicatorList)
        {
            sInd.SetActive(true);
        }
    }
    public void Hide()
    {
        foreach(GameObject ind in indicatorList)
        {
            ind.SetActive(false);
        }
        foreach(GameObject sInd in specialIndicatorList)
        {
            sInd.SetActive(false);
        }
    }
}