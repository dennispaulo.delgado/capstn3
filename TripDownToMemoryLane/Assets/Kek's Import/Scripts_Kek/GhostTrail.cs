using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostTrail : MonoBehaviour
{
    public GameObject ghostPrefab;
    public float delay = 1.0f;
    float delta = 0;

    [SerializeField] GameObject player;
    public float destroyTime = 0.1f;

    // Start is called before the first frame update
    void Start()
    {
        player.GetComponent<PlayerMovements>();
    }

    // Update is called once per frame
    void Update()
    {
        if (delta > 0) { delta -= Time.deltaTime; }
        else { delta = delay; CreateGhost(); }
    }

    private void CreateGhost()
    {
        GameObject ghostObj = Instantiate(ghostPrefab, transform.position + new Vector3(0, -1f, 0), transform.rotation);
        ghostObj.transform.localScale = player.transform.localScale;
        Destroy(ghostObj, destroyTime);

    }
}
