using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuidedJump : MonoBehaviour
{
    public static GuidedJump Instance;
    [SerializeField] private GameObject guidePrefab;
    [SerializeField] private LayerMask ground;


    private void Awake()
    {
        Instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;

        if (Physics.Raycast(this.transform.position, Vector3.down, out hit, 50f, ground))
        {

            if (hit.collider.tag == "Ground")
            {

                var distance = Vector3.Distance(hit.point, this.transform.position);
                //Debug.Log(distance);
                if (distance > 1.7)
                {
                    //guidePrefab.SetActive(true);
                    GuideOn();
                    guidePrefab.transform.position = hit.point + new Vector3(0, 0.4f, 0);
                }
               

            }
            else
            {
                GuideOff();
            }

            
           

        }
        else
        {
            GuideOff();
        }
    }

    public void GuideOff()
    {
        Debug.Log("is called");
        guidePrefab.SetActive(false);
    }

    public void GuideOn()
    {
        guidePrefab.SetActive(true);
    }


}
