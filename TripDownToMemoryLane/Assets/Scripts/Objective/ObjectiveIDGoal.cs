using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectiveIDGoal : Goal
{
    public int objectiveID;
    public ObjectiveIDGoal(string des, int id, GoalType gt)
    {
        //objective = obj;
        description = des;
        objectiveID = id;
        goalType = gt;
        Init();
    }

    public override void Init()
    {
        base.Init();
        ObjectiveEvents.instance.onObjectiveArrived += CheckObjectiveID;
    }

    public void CheckObjectiveID(int id)
    {
        if (id == objectiveID)
        {
            isCompleted = true;
        }
        ObjectiveEvents.instance.CheckObjectives();
    }
}
