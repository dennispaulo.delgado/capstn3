using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IFeedback
{
    void OnComplete();

    void OnDeactivate();
}
