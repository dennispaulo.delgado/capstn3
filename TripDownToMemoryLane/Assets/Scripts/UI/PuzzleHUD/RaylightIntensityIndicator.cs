using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RaylightIntensityIndicator : MonoBehaviour
{
    private Transform rayLightPosition;

    private RectTransform canvas;
    private Vector2 uiOffset;
    public bool isPlayerActive = false;
    [Header("ActiveAndInactiveReferences")]
    public Transform inActiveIndicator;
    public Transform activeIndicator;
    [Header("ImageReferences")]
    //private Image indicator;
    public Image number;

    [Header("Sprites")]
    public List<Sprite> intensityNumberSprites;

    public IntensityControl rayLightIntensityControl;
    private void Update()
    {
        SetIndicatorPosition();
        if (isPlayerActive)
        {
            CheckForActiveIntensity();
        }
    }
    private void SetIndicatorPosition()
    {
        Vector2 ViewportPosition = Camera.main.WorldToViewportPoint(rayLightPosition.position);
        Vector2 proportionalPosition = new Vector2(ViewportPosition.x * canvas.sizeDelta.x, ViewportPosition.y * canvas.sizeDelta.y);

        this.GetComponent<RectTransform>().localPosition = proportionalPosition - uiOffset;
    }

    public void SetReferences(Transform rayLPositon, RectTransform cnvs, IntensityControl rayLghtIntCon, bool isRLActive)
    {
        rayLightIntensityControl = rayLghtIntCon;
        rayLightPosition = rayLPositon;
        canvas = cnvs;
        uiOffset = new Vector2((float)canvas.sizeDelta.x / 2f, (float)canvas.sizeDelta.y / 2f);
        if(isRLActive)
        {
            ActiveRayLight();
            CheckForActiveIntensity();
        }
        else
        {
            InActiveRayLight();
            CheckForIntensity();
        }
        SetIndicatorPosition();
    }
    public void InActiveRayLight()
    {
        inActiveIndicator.gameObject.SetActive(true);
        activeIndicator.gameObject.SetActive(false);
        //indicator.sprite = inactiveRL;
        //indicator = inActiveIndicator.GetChild(0).GetComponent<Image>();
        number = inActiveIndicator.GetChild(1).GetComponent<Image>();
    }

    public void ActiveRayLight()
    {
        inActiveIndicator.gameObject.SetActive(false);
        activeIndicator.gameObject.SetActive(true);
        //indicator.sprite = activeRL;
        //indicator = activeIndicator.GetChild(0).GetComponent<Image>();
        number = activeIndicator.GetChild(1).GetComponent<Image>();
        isPlayerActive = true;
    }

    private void CheckForActiveIntensity()
    {
        if (rayLightIntensityControl != null && number != null)
        {
            number.sprite = intensityNumberSprites[rayLightIntensityControl.ReturnCurrentIntensity() - 1];
        }
    }

    public void CheckForIntensity()
    {
        if (rayLightIntensityControl != null&&number!=null)
        {
            number.sprite = intensityNumberSprites[rayLightIntensityControl.ReturnIntensityNeeded()-1];
        }
    }

}
