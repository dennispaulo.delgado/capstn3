using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="Equipment/ApertureControl")]
public class ApertureControl : Equipment
{
    public List<float> apertureSizes;
    public List<float> apertureSizeFailure;
    private bool resettedEquipment = true;
    public override void GetReference(Loadout refer)
    {
        pLoadout = refer;
    }

    public override void OnDecrease()
    {
        if (canUseEquipment)
        {
            ChangeApertureIndex(-1);
            PlayerHUD.instance.OnSliderClick(equipmentType,AbilitySlider.Decrease);
        }
    }

    public override void OnEquipped()
    {
        //currentIndexes = pLoadout.ReturnAbilityIndex(PlayerAbility.ApertureControl);
        PlayerHUD.instance.MakeLogoActive(PlayerAbility.ApertureControl);
        PlayerHUD.instance.ChangeSliderDisplay(equipmentType, (apertureSizes[currentIndexes]).ToString());
    }

    public override void OnIncrease()
    {
        if (canUseEquipment)
        {
            ChangeApertureIndex(1);
            PlayerHUD.instance.OnSliderClick(equipmentType, AbilitySlider.Increase);
        }
    }

    public override void OnUnEquipped()
    {
    }

    public override void OnUse()
    {
    }

    private void ChangeApertureIndex(int i)
    {
        if (apertureSizes.Count <= 0)
        {
            return;
        }
        else if (apertureSizes.Count == 1)
        {
            currentIndexes = 0;
        }
        else if (apertureSizes.Count > 1)
        {
            if (currentIndexes + i < 0)
            {
                currentIndexes = 0;
            }
            else if (currentIndexes + i > apertureSizes.Count - 1)
            {
                currentIndexes = apertureSizes.Count - 1;
            }
            else
            {
                currentIndexes += i;
            }
        }
        ChangeApertureSize(currentIndexes);
    }

    private void ChangeApertureSize(int index)
    {
        EquipmentChangesEvents.instance.ChangeApertureSize(apertureSizes[index]);
        PlayerHUD.instance.ChangeSliderDisplay(equipmentType, (apertureSizes[index]).ToString());
        //pLoadout.SetAbilityIndex(PlayerAbility.ApertureControl, index);
    }

    public override void ResetEquipment()
    {
        currentIndexes = defaultIndex;
        EquipmentChangesEvents.instance.ChangeApertureSize(apertureSizes[currentIndexes]);
        if (pLoadout.currentEquipment.equipmentType == equipmentType)
        {
            PlayerHUD.instance.ChangeSliderDisplay(equipmentType, (apertureSizes[currentIndexes]).ToString());
        }
        //pLoadout.SetAbilityIndex(PlayerAbility.ApertureControl, defaultIndex);

    }

    public override void EquipmentCooldown()
    {
        PlayerHUD.instance.UpdateCooldownHUD(PlayerAbility.ApertureControl, curFailCoolDown / defFailCoolDown);
        if (currentIndexes != defaultIndex && canUseEquipment)
        {
            curFailCoolDown -= Time.deltaTime;
            if (curFailCoolDown <= 0)
            {
                canUseEquipment = false;
                resettedEquipment = false;
                EquipmentChangesEvents.instance.ChangeApertureSize(-10);
            }
        }
        else
        {
            curFailCoolDown += Time.deltaTime;
            if (curFailCoolDown >= defFailCoolDown)
            {
                curFailCoolDown = defFailCoolDown;
                if (!resettedEquipment)
                {
                    ResetEquipment();
                    canUseEquipment = true;
                    resettedEquipment = true;
                }
            }
        }
    }

    public override void ResetToDefault()
    {
        ChangeApertureSize(defaultIndex);
    }
}
