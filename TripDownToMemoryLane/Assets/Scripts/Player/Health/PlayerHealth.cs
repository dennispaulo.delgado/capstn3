using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    public int playerHP;
    public int playerCurHP;
    public bool playerIsInvulnerable = false;
    public float invulnerabilityDuration;
    public float durationForBlinkAnim;
    public float splatDuration;
    public GameObject model;

    void Start()
    {
        PlayerHUD.instance.UpdatePlayerHealthBar(playerCurHP);
        GameManager.instance.SetRespawnPoint(this.transform.position);
    }

    void Update()
    {
        
    }

    public void PlayerTakeDamage(int dam)
    {
        if (!playerIsInvulnerable)
        {
            playerCurHP -= dam;
            SoundManager.PlaySound(SoundManager.Sound.PlayerDamage, .5f);
            PlayerHUD.instance.UpdatePlayerHealthBar(playerCurHP);
            StartCoroutine("MakePlayerInvulnerable");
            if (playerCurHP <= 0)
            {
                StopAllCoroutines();
                PlayerDeath();
            }
        }
    }

    private void PlayerDeath()
    {
        StopAllCoroutines();
        GameManager.instance.PlayerIncapacitated(this.gameObject);
    }

    public void FullHealPlayer()
    {
        StopAllCoroutines();
        model.gameObject.SetActive(true);
        playerIsInvulnerable = false;
        playerCurHP = playerHP;
        this.GetComponent<PlayerMovements>().model.localScale = new Vector3(1, 1, 1);
        this.GetComponent<PlayerMovements>().CanMove = true;
        PlayerHUD.instance.UpdatePlayerHealthBar(playerCurHP);
    }

    public void SquashPlayer()
    {
        this.GetComponent<PlayerHealth>().PlayerTakeDamage(1);
        if (playerCurHP > 0)
        {
            StartCoroutine("SplatPlayer");
        }
    }

    IEnumerator SplatPlayer()
    {
        this.GetComponent<PlayerMovements>().model.localScale = new Vector3(2, 0.1f, 2);
        this.GetComponent<PlayerMovements>().CanMove = false;
        yield return new WaitForSeconds(splatDuration);
        this.GetComponent<PlayerMovements>().model.localScale = new Vector3(1, 1, 1);
        this.GetComponent<PlayerMovements>().CanMove = true; 
        StartCoroutine("MakePlayerInvulnerable");
    }

    IEnumerator PlayerInvulnerableAnimation()
    {
        yield return new WaitForSeconds(durationForBlinkAnim);
        if(model.activeSelf)
        {
            model.SetActive(false);
        }
        else
        {
            model.SetActive(true);
        }
        StartCoroutine("PlayerInvulnerableAnimation");
    }
    IEnumerator MakePlayerInvulnerable()
    {
        playerIsInvulnerable = true;
        StartCoroutine("PlayerInvulnerableAnimation");
        yield return new WaitForSeconds(invulnerabilityDuration);
        StopCoroutine("PlayerInvulnerableAnimation");
        model.SetActive(true);
        playerIsInvulnerable = false;
    }
}
