using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Cinemachine;

[System.Serializable]
public class OnStartScene : UnityEvent
{

}

[System.Serializable]
public class OnMultipleCamera : UnityEvent
{

}

public class CameraTransition : MonoBehaviour
{
    [SerializeField] List<CinemachineVirtualCamera> _cameraList; 

    public OnStartScene onStartSceneCam;
    public OnMultipleCamera onMultipleCamera;
    public static CameraTransition Instance;
    public CinemachineVirtualCamera _playerCamera;
    public CinemachineVirtualCamera _activeCamera;



    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        onStartSceneCam?.Invoke();
    }

    public void Transition(CinemachineVirtualCamera camera)
    {
        StartCoroutine(CamTransition(camera));
    }

    public void TransitionWithDelay(CinemachineVirtualCamera camera)
    {
        StartCoroutine(CamTransitionWithDelay(camera));
    }

    public void StartSceneTransition()
    {
        StartCoroutine(StartSceneCamTransition(_cameraList));
    }

    public IEnumerator CamTransition(CinemachineVirtualCamera camera)
    {
        camera.Priority = 11;

        yield return new WaitForSeconds(3f);

        camera.Priority = 9;
    }


    public IEnumerator CamTransitionWithDelay(CinemachineVirtualCamera camera)
    {
        yield return new WaitForSeconds(2.5f);

        camera.Priority = 11;

        yield return new WaitForSeconds(3f);

        camera.Priority = 9;
    }


    public IEnumerator StartSceneCamTransition(List<CinemachineVirtualCamera> cameraList)
    {
        //Wait Delay

        yield return new WaitForSeconds(0.25f);

        cameraList[0].Priority = 11;

        yield return new WaitForSeconds(3f);

        cameraList[0].Priority = 9;
        cameraList[1].Priority = 11;

        yield return new WaitForSeconds(3f);

        cameraList[1].Priority = 9;

        


    }

    public void OneWayTransition(CinemachineVirtualCamera camera)
    {
        if(_activeCamera != null)
        {
            _activeCamera = camera;
        }
        camera.Priority = 11;
    }

    public void ReturnToPlayerCam()
    {
        if(_activeCamera != _playerCamera)
        {
            _activeCamera.Priority = 9;
            _activeCamera = _playerCamera;
            _activeCamera.Priority = 11;
        }       
    }

    public void MultipleCameraPan()
    {
        onMultipleCamera?.Invoke();
    }
}
