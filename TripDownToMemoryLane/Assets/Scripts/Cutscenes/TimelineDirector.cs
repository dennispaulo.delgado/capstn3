using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Timeline;
using UnityEngine.Playables;

public class TimelineDirector : MonoBehaviour
{
    private PlayableDirector plyblDrctr;
    private void Start()
    {
        plyblDrctr = GetComponent<PlayableDirector>();
    }
    public void PausePlayableDirector()
    {
        plyblDrctr.Pause();
    }

    public void ResumrPlayableDirector()
    {
        plyblDrctr.Resume();
    }
}
