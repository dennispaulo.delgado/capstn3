using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PressurePlate : MonoBehaviour
{
    public float plateDropDistance;
    public GameObject plate;
    public PresPlatActivated objectToActivate;
    public bool plateActive = false;

    private Vector3 plateOriginalPos;

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Vector3 wireCube = new Vector3(plate.transform.localScale.x, plate.transform.localScale.y, plate.transform.localScale.z);
        Gizmos.DrawWireCube(this.transform.position + (this.transform.up * plateDropDistance * -1), wireCube);
    }

    void Start()
    {
        GetComponent<BoxCollider>().size = plate.transform.localScale;
        plateOriginalPos = plate.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if(plateActive&&objectToActivate!=null)
        {
            objectToActivate.Activated();
        }
        else if(!plateActive&&objectToActivate!=null)
        {
            objectToActivate.Deactivate();
        }
    }

    private void OnTriggerStay(Collider other)
    {
        plate.transform.position = new Vector3(plate.transform.position.x, Mathf.Lerp(plate.transform.position.y, plateOriginalPos.y - plateDropDistance, 0.5f), plate.transform.position.z);
        plateActive = true;
    }

    private void OnTriggerExit(Collider other)
    {
        plate.transform.position = new Vector3(plate.transform.position.x, Mathf.Lerp(plate.transform.position.y, plateOriginalPos.y, 0.5f), plate.transform.position.z);
        plateActive = false;
    }
}
