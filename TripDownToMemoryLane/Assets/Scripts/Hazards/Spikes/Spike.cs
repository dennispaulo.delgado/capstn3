using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spike : MonoBehaviour
{
    [Header("Attribute")]
    public SpikeHazard sHazard;
    void Start()
    {
        
    }

    void Update()
    {
        
    }


    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            sHazard.Bumped(collision.gameObject.GetComponent<Rigidbody>(), this.transform.position);
            CameraShake.Instance.ShakeCamera(1f, .5f);
        }
    }
}
