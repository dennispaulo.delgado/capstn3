﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class InteractionHandler : MonoBehaviour
{

    private PlayerControls controls;
    private DialogueInput dialInputs;

    private void Awake()
    {
        SingletonManager.Register<InteractionHandler>(this);
        controls = new PlayerControls();
        dialInputs = new DialogueInput();
    }

    private void OnEnable()
    {
        controls.Enable();
    }

    private void OnDisable()
    {
        controls.Disable();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public DialogueInput GetDialogueInputs()
    {
        return dialInputs;
    }
    public PlayerControls GetPlayerControls()
    {
        return controls;
    }
}
