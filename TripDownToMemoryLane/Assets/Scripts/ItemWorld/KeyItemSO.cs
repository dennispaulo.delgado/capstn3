using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObject/Key")]
public class KeyItemSO : ItemSO
{
    public int itemKeyID;
}
