﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class XSectoDestroy : MonoBehaviour
{
    public float secondsToDestroy;
    private void Update()
    {
        secondsToDestroy -= Time.deltaTime;
        if(secondsToDestroy<=0)
        {
            Destroy(this.gameObject);
        }
    }
}
