using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Goal
{
    //public Objective objective;
    public string description;
    public bool isCompleted = false;
    public GoalType goalType;
    [Header("Required for Collect Objective")]
    public int currentAmount;
    public int requiredAmount;

    public virtual void Init()
    {

    }


    public void Evaluate()
    {
        if (currentAmount >= requiredAmount)
        {
            Complete();
        }
    }

    private void Complete()
    {
        isCompleted = true;
    }
}

