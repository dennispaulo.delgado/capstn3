using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuButton : MonoBehaviour
{
    public MainMenu mainMenu;

    public Sprite unselected;
    public Sprite selected;
    public Sprite description;

    public void OnHoverMouse()
    {
        GetComponent<Image>().sprite = selected;
        if (description != null)
        {
            mainMenu.ChangeButtonDescription(description);
        }
    }
    public void OnUnHoverMouse()
    {
        GetComponent<Image>().sprite = unselected;
        if (description != null)
        {
            mainMenu.ResetButtonDescription();
        }
    }
}