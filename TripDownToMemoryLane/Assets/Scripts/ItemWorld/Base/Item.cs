using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Item
{
    public string name;
    public string description;
    public int amount;
    public ItemType type;
}

public enum ItemType
{
    Regular,
    Key
}
