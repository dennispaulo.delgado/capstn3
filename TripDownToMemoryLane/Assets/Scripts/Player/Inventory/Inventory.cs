using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    public List<Item> inventory = new List<Item>();
    public List<KeyItem> keyInventory = new List<KeyItem>();




    public void AddRegularItem(RegularItem itemToGive)
    {
        inventory.Add(itemToGive);

        if(itemToGive.name == "Props")
        {
            GameEvent.current.OnCollectedProps();
        }
    }
    public void AddKeyItem(KeyItem itemToGive)
    {
        keyInventory.Add(itemToGive);
    }


}
