using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="Equipment/White Balance")]
public class WhiteBalanceEqpmt : Equipment
{
    public List<ColorChoices> whiteBalanceColor;
    private bool resettedEquipment = true;
    [Header("EquipmentFailure")]
    public float wbChangeRate;
    public override void GetReference(Loadout refer)
    {
        pLoadout = refer;
    }

    public override void OnDecrease()
    {
        if (canUseEquipment)
        {
            ChangeWhiteBalanceIndex(-1);
            PlayerHUD.instance.OnSliderClick(equipmentType, AbilitySlider.Decrease);
        }
    }

    public override void OnEquipped()
    {
        //currentIndexes = pLoadout.ReturnAbilityIndex(PlayerAbility.WhiteBalance);
        PlayerHUD.instance.MakeLogoActive(PlayerAbility.WhiteBalance);
        PlayerHUD.instance.ChangeSliderDisplay(equipmentType, (whiteBalanceColor[currentIndexes].colorEnum).ToString());
    }

    public override void OnIncrease()
    {
        if (canUseEquipment)
        {
            ChangeWhiteBalanceIndex(1);
            PlayerHUD.instance.OnSliderClick(equipmentType, AbilitySlider.Increase);
        }
    }

    public override void OnUnEquipped()
    {
    }

    public override void OnUse()
    {
    }
    private void ChangeWhiteBalanceIndex(int i)
    {
        if (whiteBalanceColor.Count <= 0)
        {
            return;
        }
        else if (whiteBalanceColor.Count == 1)
        {
            currentIndexes = 0;
        }
        else if (whiteBalanceColor.Count > 1)
        {
            if (currentIndexes + i < 0)
            {
                currentIndexes = 0;
            }
            else if (currentIndexes + i > whiteBalanceColor.Count - 1)
            {
                currentIndexes = whiteBalanceColor.Count - 1;
            }
            else
            {
                currentIndexes += i;
            }
        }
        ChangeWhiteBalance(currentIndexes);
    }

    private void ChangeWhiteBalance(int index)
    {
        GlobalVolumeManager.instance.ChangeWhiteBalance(whiteBalanceColor[index]);
        PlayerHUD.instance.ChangeSliderDisplay(equipmentType, (whiteBalanceColor[index].colorEnum).ToString());
        //pLoadout.SetAbilityIndex(PlayerAbility.WhiteBalance, index);
    }

    public override void ResetEquipment()
    {
        currentIndexes = defaultIndex;
        GlobalVolumeManager.instance.ChangeWhiteBalance(whiteBalanceColor[currentIndexes]);
        if (pLoadout.currentEquipment.equipmentType == equipmentType)
        {
            PlayerHUD.instance.ChangeSliderDisplay(equipmentType, (whiteBalanceColor[currentIndexes].colorEnum).ToString());
        }
        //pLoadout.SetAbilityIndex(PlayerAbility.WhiteBalance, defaultIndex);
    }

    public override void EquipmentCooldown()
    {
        PlayerHUD.instance.UpdateCooldownHUD(PlayerAbility.WhiteBalance, curFailCoolDown / defFailCoolDown);
        if (currentIndexes != defaultIndex && canUseEquipment)
        {
            curFailCoolDown -= Time.deltaTime;
            if (curFailCoolDown <= 0)
            {
                canUseEquipment = false;
                resettedEquipment = false;
                GlobalVolumeManager.instance.WhiteBalanceEquipmentFailureStart(wbChangeRate);
            }
        }
        else
        {
            curFailCoolDown += Time.deltaTime;
            if (curFailCoolDown >= defFailCoolDown)
            {
                curFailCoolDown = defFailCoolDown;
                if (!resettedEquipment)
                {
                    GlobalVolumeManager.instance.WhiteBalanceEquipmentFailureEnd();
                    ResetEquipment();
                    canUseEquipment = true;
                    resettedEquipment = true;
                }
            }
        }
    }

    public override void ResetToDefault()
    {
        ChangeWhiteBalance(defaultIndex);
    }
}
