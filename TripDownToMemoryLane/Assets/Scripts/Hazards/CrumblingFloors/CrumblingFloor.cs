using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrumblingFloor : MonoBehaviour
{
    [Header("Attributes")]
    public Animator platformAnim;
    public float secBeforeCrumbling;
    public float secToRecover;
    [Header("HiddenAttributes")]
    private bool isCrumbled = false;
    private bool isActive = false;
    private float defSecBeforeCrumbling;
    private float defSecToRecover;
    void Start()
    {
        defSecBeforeCrumbling = secBeforeCrumbling;
        defSecToRecover = secToRecover;
    }

    void Update()
    {
        if (isActive)
        {
            if (!isCrumbled)
            {
                if (secBeforeCrumbling > 0)
                {
                    secBeforeCrumbling -= Time.deltaTime * TimeManager.instance.timeMultiplier * TimeManager.instance.timeScale;
                }
                else
                {
                    PlatformCrumbled();
                    secBeforeCrumbling = defSecBeforeCrumbling;
                    isCrumbled = true;
                }
            }
            
        }
        else
        {
            if(isCrumbled)
            {
                if (secToRecover > 0)
                {
                    secToRecover -= Time.deltaTime * TimeManager.instance.timeMultiplier * TimeManager.instance.timeScale;
                }
                else
                {
                    PlatformRecover();
                    secToRecover = defSecToRecover;
                    isCrumbled = false;
                }
            }
        }
    }

    private void PlatformCrumbled()
    {
        platformAnim.SetBool("isNeutral", false);
        platformAnim.SetBool("isCrumbled", true);
    }

    private void PlatformRecover()
    {
        platformAnim.SetBool("isNeutral", false);
        platformAnim.SetBool("isCrumbled", false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            isActive = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            isActive = false;
        }
    }
}
