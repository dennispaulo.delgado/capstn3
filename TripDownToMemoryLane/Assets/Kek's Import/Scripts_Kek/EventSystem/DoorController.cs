﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorController : MonoBehaviour
{
    public int id;
    public List<GameObject> lightFeedback;
    


    bool isActive;
    Vector3 originPosition;
    IntensitySphere intensitySphere;

    // Start is called before the first frame update
    void Start()
    {
        originPosition = transform.localPosition;
        GameEvent.current.onProjectorObjectTriggerEnter += OnDoorwayOpen;
        GameEvent.current.onProjectorObjectTriggerExit += OnDoorwayClose;
        //GameEvent.current.onPuzzleSlidingComplete += OnDoorwayOpen;
        GameEvent.current.onPuzzleSlidingComplete += LightFeedback;
        //intensitySphere.OnLightIntensityHit += IntensityDoorOpen;
        //intensitySphere.OnLightIntensityExit += IntensityDoorClose;


    }


    private void OnDoorwayOpen(int id)
    {
        if(id == this.id)
        {
            LeanTween.moveLocalY(gameObject, originPosition.y -5f, 1).setEaseOutQuad();


        }

    }

    private void OnDoorwayClose(int id)
    {

        if(id == this.id)
        {
            LeanTween.moveLocalY(gameObject, originPosition.y, 1f).setEaseInQuad();
        }

    }

    public void OnLightActivateDoor()
    {
        LeanTween.moveLocalY(gameObject, originPosition.y, 1f).setEaseInQuad();
    }

    private void LightFeedback(int id)
    {
        if (id == this.id)
        {
            foreach (var obj in lightFeedback)
            {
                obj.GetComponent<MeshRenderer>().material.color = Color.green;
            }

        }

    }

    public void IntensityDoorOpen(int intensityMeter)
    {
        
        if(id == intensityMeter)
        {
            LeanTween.moveLocalY(gameObject, originPosition.y - 5f, 1).setEaseOutQuad();
            
        }
        else
        {
            IntensityDoorClose(intensityMeter);
        }
    }

    public void IntensityDoorClose(int intensityMeter)
    {
        if (id != intensityMeter)
        {
            LeanTween.moveLocalY(gameObject, originPosition.y, 1f).setEaseInQuad();
        }
    }

}
