using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SoundAssets : MonoBehaviour
{
    public SoundAudioClip[] soundAudioClipArray;

    public static SoundAssets instance;

    private void Awake()
    {
        instance = this;
    }

    [System.Serializable]
    public class SoundAudioClip
    {
        public SoundManager.Sound sound;
        public List<AudioClip> audioClipList;
    }




    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
