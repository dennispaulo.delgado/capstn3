﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;




public class PlayerInteract : MonoBehaviour
{
    public bool isInteracting;
    public bool isInteractingSliding;

    [SerializeField] private bool canInteract;
    [SerializeField] GameObject puzzleObject;
    //[SerializeField] GameObject testObject;
    //[SerializeField] LayerMask blockMask;

    private Camera camera;
    private int puzzleId;

    [Header("DetectingInteractables")]
    public LayerMask interactableMasks;
    public float interactableLength;
    public GameObject nearestInteractable;

    public bool CanInteract { get => canInteract; set => canInteract = value; }

    private InteractionHandler interactionHandler;

    [SerializeField] private bool canPause = true;

    private void Awake()
    {
        camera = Camera.main;
        //blockMask = LayerMask.GetMask("Block");
    }

    // Start is called before the first frame update
    void Start()
    {
        interactionHandler = SingletonManager.Get<InteractionHandler>();
        interactionHandler.GetPlayerControls().PlayerInteraction.InteractItem.performed += _ => InteractPuzzle();
        interactionHandler.GetPlayerControls().PlayerInteraction.ExitPuzzle.performed += _ => ExitPuzzle();
        interactionHandler.GetPlayerControls().PlayerInteraction.ExitPuzzle.performed += _ => PauseOrUnPauseGame();
        interactionHandler.GetPlayerControls().Mouse.Click.performed += _ => DetectPuzzle();
        //interactionHandler.GetPlayerControls().Mouse.Click.performed += _ => OnMousePress();
        interactionHandler.GetPlayerControls().PlayerInteraction.InteractItem.performed += _ => InteractItem();
        interactionHandler.GetPlayerControls().PlayerInteraction.AutoCompletePuzzle.performed += _ => CompleteSlidingPuzzle();
        GameEvent.current.onRayLightInteract += InteractRayLights;
        GameEvent.current.onRayLightExit += ExitRayLights;
        GameEvent.current.onItemPickup += PlayPickUpVFX;
        GameEvent.current.onCollectedKey += PlayKeyVFX;
    }

    public void CompleteSlidingPuzzle()
    {
        if(puzzleObject == null) { return; }

        if (puzzleObject.GetComponent<SlidingPuzzle>())
        {
            puzzleObject.GetComponent<SlidingPuzzle>().ShowFullSlidingPuzzle();
        }
    }


    // Update is called once per frame
    void Update()
    {
        CheckInteractables();
        CheckItemIndication();


    }

    //public void OnMousePress()
    //{
    //    Debug.Log("MousePress");
    //    Ray ray = Camera.main.ScreenPointToRay(interactionHandler.GetPlayerControls().Mouse.Position.ReadValue<Vector2>());

    //    if (Physics.Raycast(ray, out RaycastHit raycastHit, 1000f))
    //    {
    //        testObject.transform.position = raycastHit.point;
    //        Debug.Log(raycastHit.collider.name);
    //    }


    //}
    private void PauseOrUnPauseGame()
    {
        if(canPause)
            PauseMenu.instance.PauseOrUnpauseGame();
    }



    private void CheckItemIndication()
    {

        Collider[] interactableColliders = Physics.OverlapSphere(this.transform.position, 3, interactableMasks);
        foreach (Collider i in interactableColliders)
        {
            if(i.gameObject != nearestInteractable)
            {
                InteractableManager.instance.RemoveIndicator( i.transform);
                //InteractableManager.instance.IndicatorInRange(this.transform, i.transform);
            }
        }

    }

    private void CheckInteractables()
    {
        Collider[] interactableColliders = Physics.OverlapSphere(this.transform.position, interactableLength, interactableMasks);
        if (interactableColliders.Length == 1)
        {
            nearestInteractable = interactableColliders[0].gameObject;
            InteractableManager.instance.IndicatorIsNearest(this.transform, nearestInteractable.transform);
        }
        else if (interactableColliders.Length > 1)
        {
            GameObject tempNear = interactableColliders[0].gameObject;
            for (int i = 1; i < interactableColliders.Length; i++)
            {
                if (CheckDistance(this.transform, interactableColliders[i].transform) < CheckDistance(this.transform, tempNear.transform))
                {
                    tempNear = interactableColliders[i].gameObject;
                }
            }
            nearestInteractable = tempNear.gameObject;
            InteractableManager.instance.IndicatorIsNearest(this.transform, tempNear.transform);
        }
        else
        {
            nearestInteractable = null;
            InteractableManager.instance.ClearIndicators();
        }
    }
    private float CheckDistance(Transform object1, Transform object2)
    {
        return Vector3.Distance(object1.position, object2.position);
    }

    public void PuzzleDetected(GameObject puzzleObject)
    {
        this.puzzleObject = puzzleObject;
    }

    public void SetPuzzleToNull()
    {
        this.puzzleObject = null;
    }

    private void InteractItem()
    {
        if (nearestInteractable != null)
        {
            if (nearestInteractable.GetComponent<RegularItemWorld>() != null)
            {
                this.GetComponent<Inventory>().AddRegularItem(nearestInteractable.GetComponent<RegularItemWorld>().ReturnRegularItem());
                GameEvent.current.OnItemPickup(nearestInteractable.GetComponent<RegularItemWorld>().vfx, nearestInteractable.gameObject.transform.position);

                if (nearestInteractable.GetComponent<Collectible>())
                {
                    DoorCollectibles();
                    nearestInteractable.gameObject.SetActive(false);

                }
                else
                {
                    Destroy(nearestInteractable.gameObject);
                }



            }
            if (nearestInteractable.GetComponent<KeyItemWorld>() != null)
            {
                this.GetComponent<Inventory>().AddKeyItem(nearestInteractable.GetComponent<KeyItemWorld>().ReturnKeyItem());
                GameEvent.current.OnCollectedKey(nearestInteractable.GetComponent<KeyItemWorld>().vfx, nearestInteractable.gameObject.transform.position);
                //Destroy(nearestInteractable.gameObject);
                if (nearestInteractable.GetComponent<Collectible>())
                {
                    DoorCollectibles();
                    nearestInteractable.gameObject.SetActive(false);
                }
                else
                {
                    Destroy(nearestInteractable.gameObject);
                }


            }
        }
        
    }

    private bool CheckForItemBlocker()
    {
        Debug.DrawRay(transform.position, (transform.position - nearestInteractable.transform.position), Color.red);
            RaycastHit[] hits;
        hits = Physics.RaycastAll(transform.position, (nearestInteractable.transform.position - transform.position), 3);
        if (hits.Length <= 0)
        {
            return false;
        }
        if (hits.Length > 0)
        {
            if (hits[0].collider.gameObject == this.gameObject)
            {
                if (hits[1].transform.gameObject == nearestInteractable.gameObject)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else if (hits[0].transform.gameObject == nearestInteractable.gameObject)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        else
        {
            return true;
        }
    }


    private void DoorCollectibles()
    {
        var propForDoor = nearestInteractable.GetComponent<Collectible>();
        propForDoor.ItemPickUp();

       
    }

    private void InteractPuzzle()
    {
        if (canInteract != false)
        {
            isInteracting = true;
            canPause = false;
            if (GetComponent<PlayerMovements>().CanMove)
            {
                if (puzzleObject.GetComponent<PuzzleCamera>())
                {
                    isInteractingSliding = true;
                    GetComponent<PlayerMovements>().SetCantMove();
                    puzzleObject.GetComponent<PuzzleCamera>().GetPuzzleCamera().Priority = 11;
                    PlayerHUD.instance.ShowSlidingPuzzleHUD();
                    InteractableManager.instance.Hide();
                }

                if (puzzleObject.GetComponent<RaylightPuzzle>())
                {
                    GetComponent<PlayerMovements>().SetCantMove();
                    var rayLightPuzzle = puzzleObject.GetComponent<RaylightPuzzle>();

                    GameEvent.current.RayLightInteract();
                    PlayerHUD.instance.ShowRayLightPuzzleHUD(puzzleObject.gameObject, puzzleObject.transform);
                    InteractableManager.instance.Hide();
                }
            }
            

        }

    }

    private void CanPauseTrue()
    {
        canPause = true;
    }

    public void PublicExitPuzzle()
    {
        ExitPuzzle();
    }

    private void ExitPuzzle()
    {
        if(canInteract != false)
        {
            isInteracting = false;
            Invoke("CanPauseTrue", .5f);
            GetComponent<PlayerMovements>().CanMove = true;
            PlayerHUD.instance.UnHideHUD();
            InteractableManager.instance.UnHide();
            if (puzzleObject.GetComponent<PuzzleCamera>())
            {
                puzzleObject.GetComponent<PuzzleCamera>().GetPuzzleCamera().Priority = 9;
                if (puzzleObject.GetComponent<SlidingPuzzle>())
                {
                    isInteractingSliding = false;
                    if (puzzleObject.GetComponent<SlidingPuzzle>().GetIsSolved() == true)
                    {
                        if (puzzleObject.GetComponent<TransitionPuzzleNoMemory>() == null)
                            return;
                        var cameraToPan = puzzleObject.GetComponent<TransitionPuzzleNoMemory>().GetCamera();
                        CameraTransition.Instance.TransitionWithDelay(cameraToPan);
                    }
                }


            }

            if (puzzleObject.GetComponent<RaylightPuzzle>())
            {
                isInteractingSliding = false;
                var rayLightPuzzle = puzzleObject.GetComponent<RaylightPuzzle>();

                GameEvent.current.RayLightExit();
            }

        }
    }

    private void DetectPuzzle()
    {
        if(!GetComponent<PlayerMovements>().CanMove)
        {
            Ray ray = camera.ScreenPointToRay(interactionHandler.GetPlayerControls().Mouse.Position.ReadValue<Vector2>());

            RaycastHit hit;
            if(Physics.Raycast(ray, out hit))
            {
                if(hit.collider.GetComponent<Block>())
                {
                    hit.collider.GetComponent<Block>().OnMouseDown();

                }
            }

        }
    }

    private void InteractRayLights()
    {
        if (!GetComponent<PlayerMovements>().CanMove)
        {
            puzzleObject.GetComponent<RaylightPuzzle>().CanMove = true;
        }
    }

    private void ExitRayLights()
    {
        puzzleObject.GetComponent<RaylightPuzzle>().CanMove = false;
    }


    private void PlayPickUpVFX(GameObject vfx, Vector3 position)
    {
        SoundManager.PlaySound(SoundManager.Sound.PropSound, 0.4f);
        Instantiate(vfx, position, Quaternion.Euler(new Vector3(-90, 0,0)));
    }

    private void PlayKeyVFX(GameObject vfx, Vector3 position)
    {
        SoundManager.PlaySound(SoundManager.Sound.PropSound, 0.4f);
        Instantiate(vfx, position, Quaternion.Euler(new Vector3(-90, 0, 0)));
    }
}
