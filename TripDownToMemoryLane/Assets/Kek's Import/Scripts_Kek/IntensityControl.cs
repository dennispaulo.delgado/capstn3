using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntensityControl : MonoBehaviour
{
    public event Action onIntensityChanged;

    [SerializeField] [Range(1, 5)] int _intensityMeter = 3;
    [SerializeField] [Range(1, 5)] int _intensityNeeded = 3;
    [SerializeField] Material _rayLightMat;
    [SerializeField] [ColorUsage(true, true)] List<Color> _color;
    

    private bool _canMove;
    private RaylightPuzzle _rayLightPuzzle;
    private InteractionHandler _interactionHandler;

    
    private void Awake()
    {
        _rayLightPuzzle = GetComponent<RaylightPuzzle>();
        if(_rayLightMat != null)
        {
            _rayLightMat.SetColor("RayLightColor", _color[_intensityMeter - 1]);
        }

    }

    public int GetIntensityMeter()
    {
        return _intensityMeter;
    }

    public int GetIntensityNeeded()
    {
        return _intensityNeeded;
    }

    private void IncreaseIntensity()
    {
        if (_canMove)
        {
            if (_intensityMeter < 5)
            {
                _intensityMeter++;
                onIntensityChanged?.Invoke();               
            }
               

        }
           

    }


    private void DecreaseIntensity()
    {
        if (_canMove)
        {
            if (_intensityMeter > 1)
            {
                _intensityMeter--;
                onIntensityChanged?.Invoke();             
            }
        }
        
    }


    // Start is called before the first frame update
    void Start()
    {
        _interactionHandler = SingletonManager.Get<InteractionHandler>();
        _interactionHandler.GetPlayerControls().PlayerInteraction.IncreaseIntensity.performed += _ => IncreaseIntensity();
        _interactionHandler.GetPlayerControls().PlayerInteraction.DecreaseIntensity.performed += _ => DecreaseIntensity();
    }

    // Update is called once per frame
    void Update()
    {
       _canMove = _rayLightPuzzle.CanMove;
        if (_rayLightMat != null)
        {
            _rayLightMat.SetColor("RayLightColor", _color[_intensityMeter - 1]);
        }

    }

    public int ReturnIntensityNeeded()
    {
        return _intensityNeeded;
    }
    public int ReturnCurrentIntensity()
    {
        return _intensityMeter;
    }
}
