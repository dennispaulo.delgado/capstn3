using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPoint : MonoBehaviour
{
    public bool isActive = true;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (isActive)
            {
                GameManager.instance.SetRespawnPoint(other.transform.position);
                isActive = false;
            }
        }
    }
}
