using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class OnCraneInteract : UnityEvent
{

}


public class CraneInteract : MonoBehaviour
{
    public OnCraneInteract onCraneInteract;
    public bool craneNearby = false;
    public bool isAttached = false;
    [SerializeField] private Rigidbody objectToConnect;
    private Rigidbody ignoreObject;
    private InteractionHandler interactionHandler;

    public void SetObjectToConnect(Rigidbody value)
    {
        objectToConnect = value;
    }

    // Start is called before the first frame update
    void Start()
    {
        interactionHandler = SingletonManager.Get<InteractionHandler>();
        interactionHandler.GetPlayerControls().PlayerInteraction.InteractItem.performed += _ => AttachToCrane();
    }

    // Update is called once per frame
    void Update()
    {
        if (objectToConnect != null)
        {

            DistanceThreshold();
        }


    }

   

    private void DistanceThreshold()
    {
        if (CheckDistance() >= 3f)
        {
            craneNearby = false;
            objectToConnect = null;
        }
    }

    public void AttachToCrane()
    {
        if(GetComponent<PlayerInteract>().isInteracting) { return;  }

        if(objectToConnect != null && !isAttached)
        {
            
            if (ignoreObject != null)
                if (ignoreObject == objectToConnect)
                {
                    
                    return;
                }

            ignoreObject = objectToConnect;
            isAttached = true;
            var parentObject = objectToConnect.transform.parent;
            transform.parent = parentObject;

            if (!transform.gameObject.GetComponent<ConfigurableJoint>())
            {
                JointConfigurations();
            }
        }

    }

    private void JointConfigurations()
    {
        transform.gameObject.transform.rotation = Quaternion.Euler(0, 0, 0);
        transform.gameObject.AddComponent<ConfigurableJoint>();
        transform.gameObject.GetComponent<ConfigurableJoint>().xMotion = ConfigurableJointMotion.Locked;
        transform.gameObject.GetComponent<ConfigurableJoint>().yMotion = ConfigurableJointMotion.Locked;
        transform.gameObject.GetComponent<ConfigurableJoint>().zMotion = ConfigurableJointMotion.Locked;
        transform.gameObject.GetComponent<ConfigurableJoint>().autoConfigureConnectedAnchor = false;
        transform.gameObject.GetComponent<ConfigurableJoint>().connectedBody = objectToConnect;
        transform.gameObject.GetComponent<ConfigurableJoint>().connectedAnchor = new Vector3(0, -1.5f, 0);
    }

    public void DetachToCrane()
    {
        
        Invoke("IgnoreObject", 2f);
        transform.SetParent(null);
        isAttached = false;
        Destroy(GetComponent<ConfigurableJoint>());

    }

    private void IgnoreObject()
    {
        ignoreObject = null;
    }

    private float CheckDistance()
    {
        return Vector3.Distance(objectToConnect.position, transform.position);
    }
}
