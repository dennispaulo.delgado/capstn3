using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public static PauseMenu instance;

    public GameObject pauseScreen;

    public Image buttonDescription;
    public List<PauseMenuButton> pauseMenuButtons;

    private bool isPaused = false;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(this.gameObject);
        }
    }

    public void PauseOrUnpauseGame()
    {
        if(!isPaused)
        {
            Time.timeScale = 0;
            isPaused = true;
            pauseScreen.SetActive(true);
        }
        else
        {
            ReturnToGame();
        }
    }
    
    private void ResetPauseButtons()
    {
        foreach(PauseMenuButton p in pauseMenuButtons)
        {
            p.OnUnHoverMouse();
        }
    }

    public void ReturnToMainMenu()
    {
        ReturnToGame();
        SceneManager.LoadScene(0);
    }

    public void ReturnToGame()
    {
        ResetPauseButtons();
        Time.timeScale = 1;
        isPaused = false;
        pauseScreen.SetActive(false);
    }

    public void ExitGame()
    {
        Application.Quit();
    }    

    public void ChangeButtonDescription(Sprite dscrptn)
    {
        buttonDescription.gameObject.SetActive(true);
        buttonDescription.sprite = dscrptn;
    }

    public void ResetButtonDescription()
    {
        buttonDescription.gameObject.SetActive(false);
    }
}
