using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class OnActivateAllLight : UnityEvent
{

}

public class LightPressureHandler : MonoBehaviour
{
    public OnActivateAllLight onActivateAllLight;
    
    public int numberOfLightPressureNeeded;
    public int numberOfLightPressureActive;

    private bool hasActivate = false;

    private void Start()
    {
        
    }



    public void AddActiveLight()
    {
        numberOfLightPressureActive += 1;

        if(numberOfLightPressureActive >= numberOfLightPressureNeeded)
        {
            if (!hasActivate)
            {
                hasActivate = true;
                onActivateAllLight?.Invoke();
            }
        }

    }


}
