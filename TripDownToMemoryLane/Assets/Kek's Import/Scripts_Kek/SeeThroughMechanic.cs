using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.Events;

[System.Serializable]
public class OnCollideSphereMask : UnityEvent
{

}

[System.Serializable]
public class OnExitSphereMask : UnityEvent
{

}

public class SeeThroughMechanic : MonoBehaviour
{
    public OnCollideSphereMask onCollideSphereMask;
    public OnExitSphereMask onExitSphereMask;

    [SerializeField] private GameObject camera;
    [SerializeField] private GameObject target;
    [SerializeField] private LayerMask mask;

    
    

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;

        //Does ray intersects with the sphere
        if (Physics.Raycast(camera.transform.position, (target.transform.position - camera.transform.position).normalized, out hit, Mathf.Infinity, mask))
        {
           
            //if collides with sphere, scale it to 0 with DoTween
            if (hit.collider.gameObject.tag == "spheremask")
            {
                
                
                ScaleDownMask();
            }
            else
            {
                
               
                ScaleUpMask();
            }
        }
    }

    private void ScaleUpMask()
    {
        
        var tween = target.transform.DOScale(3, 2);

        if (tween.IsComplete())
        {
            
            tween.TogglePause();
        }

    }

    private void ScaleDownMask()
    {
        
        var tween = target.transform.DOScale(0, 2);

        if (tween.IsComplete())
        {
            tween.TogglePause();
        }
    }
}
