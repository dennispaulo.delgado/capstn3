using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class TutorialUI : MonoBehaviour
{
    public static TutorialUI instance;

    public UnityEvent OnFinishTutorialShow;

    public Image tutorialPrompt;

    private bool showingTutorial = false;
    private int showDuration;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(this.gameObject);
        }
    }
    void Start()
    {
        tutorialPrompt.gameObject.SetActive(false);
    }

    public void DisplayTutorial(Sprite tutorialSprite, float duration)
    {
        Debug.Log("Displaying : "+ tutorialSprite.name);
        StopAllCoroutines();
        tutorialPrompt.gameObject.SetActive(true);
        tutorialPrompt.sprite = tutorialSprite;
        tutorialPrompt.GetComponent<RectTransform>().sizeDelta = new Vector2(tutorialSprite.rect.width, tutorialSprite.rect.height);
        StartCoroutine("ShowTutorialUI",duration);
    }

    private void ResetDisplayTutorial()
    {
        tutorialPrompt.sprite = null;
        tutorialPrompt.gameObject.SetActive(false);
    }

    IEnumerator ShowTutorialUI(float duration)
    {
        yield return new WaitForSeconds(duration);
        ResetDisplayTutorial();
        OnFinishTutorialShow.Invoke();
    }

}
