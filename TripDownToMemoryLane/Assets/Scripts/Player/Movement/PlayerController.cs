using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private PlayerMovement plyrMvmnt;
    private CharacterController controller;
    private Rigidbody rb;

    [Header("MovementAttributes")]
    public float speed;
    public float jumpForce;
    private float gravity = -9.81f;
    private Vector3 velocity;
    [Header("GroundChecker")]
    public Transform groundCheck;
    public float groundCheckRadius;
    public bool isGrouded;
    public LayerMask groundLayers;
    private bool isMidAir;
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(groundCheck.position, groundCheckRadius);
    }

    private void Awake()
    {
        plyrMvmnt = new PlayerMovement();
    }
    private void OnEnable()
    {
        plyrMvmnt.Enable();
    }

    private void OnDisable()
    {
        plyrMvmnt.Disable();
    }

    void Start()
    {
        controller = GetComponent<CharacterController>();
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        plyrMvmnt.Land.Testing.performed += _ => TestingInput();
        PlayerMovement();
    }
    private void PlayerMovement()
    {
        float straight = plyrMvmnt.Land.Straight.ReadValue<float>();
        float sideway = plyrMvmnt.Land.Sideway.ReadValue<float>();
        plyrMvmnt.Land.Jump.performed += _ => PlayerJump();

        Vector3 direction = new Vector3(sideway, 0, straight).normalized;
        plyrMvmnt.Land.Dash.performed += _ => PlayerDash(direction);
        CheckIfGrounded();
        if (direction.magnitude >= 0.1f)
        {
            //controller.Move(direction * speed * Time.deltaTime);
            //rb.velocity = direction * speed * Time.deltaTime;
            transform.Translate(direction * speed * Time.deltaTime);
        }
        if (!isGrouded)
        {
            //velocity.y += gravity * Time.deltaTime;
            //transform.Translate(velocity * Time.deltaTime);
            rb.AddForce(new Vector3(0, 1.0f, 0) * rb.mass * gravity);  
        }
    }
    public void BumpedToHazard()
    {
        StopCoroutine("RemoveForce");
        StartCoroutine("RemoveForce");
    }

    private void TestingInput()
    {
        rb.velocity = Vector3.zero;
    }

    private void PlayerJump()
    {
        if(isGrouded)
        {
            rb.velocity = Vector3.up * jumpForce;
        }
    }
    private void PlayerDash(Vector3 dir)
    {
        rb.velocity = dir * jumpForce;
    }

    private void CheckIfGrounded()
    {
        isGrouded = Physics.CheckSphere(groundCheck.position, groundCheckRadius, groundLayers);
        if(isGrouded)
        {
            //rb.velocity = Vector3.zero;
        }
    }

    IEnumerator RemoveForce()
    {
        yield return new WaitForSeconds(.25f);
        rb.velocity = Vector3.zero;
    }
}
