using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[System.Serializable]
public class OnFadeInClear : UnityEvent
{

}


public class FadeTransition : MonoBehaviour
{
    public OnFadeInClear camTransition;

    public UnityEvent onFinishFadeIn;

    [SerializeField] float _fadeSpeed;

    Image _fadeInImage;
    

    // Start is called before the first frame update
    void Start()
    {
        _fadeInImage = GetComponent<Image>();
        GameEvent.current.onPuzzleSlidingCompleteTransition += StartFadeEffect;
        GameEvent.current.onPuzzleSlidingCompleteNoMemory += StartFadeEffectWithNoTransition;
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    IEnumerator FadeEffect()
    {

        yield return new WaitForSecondsRealtime(0.25f);


        float alphaIn = 0f;
        while (alphaIn < 1)
        {

            yield return null;
            alphaIn += Time.deltaTime / _fadeSpeed;
            _fadeInImage.color = new Color(1, 1, 1, alphaIn);
        }

        yield return new WaitForSecondsRealtime(1f);

        camTransition?.Invoke();


        yield return new WaitForSecondsRealtime(2f);


        float alphaOut = 1f;
        while (alphaOut > 0)
        {

            yield return null;
            alphaOut -= Time.deltaTime / _fadeSpeed;
            _fadeInImage.color = new Color(1, 1, 1, alphaOut);
        }

        yield return new WaitForSecondsRealtime(4f);
        onFinishFadeIn.Invoke();
        //SceneHandler.Instance.TransitionNextLevel();
    }

    IEnumerator FadeEffectTransition()
    {
        yield return new WaitForSecondsRealtime(0.25f);


        float alphaIn = 0f;
        while (alphaIn < 1)
        {

            yield return null;
            alphaIn += Time.deltaTime / _fadeSpeed;
            _fadeInImage.color = new Color(1, 1, 1, alphaIn);
        }

        yield return new WaitForSecondsRealtime(1f);

        SceneHandler.Instance.TransitionNextLevel();

    }

    IEnumerator FadeEffectWithNoTransition()
    {
        yield return new WaitForSeconds(0.25f);


        float alphaIn = 0f;
        while (alphaIn < 1)
        {

            yield return null;
            alphaIn += Time.deltaTime / _fadeSpeed;
            _fadeInImage.color = new Color(1, 1, 1, alphaIn);
        }

        yield return new WaitForSeconds(1f);
       

        yield return new WaitForSeconds(2f);


        float alphaOut = 1f;
        while (alphaOut > 0)
        {

            yield return null;
            alphaOut -= Time.deltaTime / _fadeSpeed;
            _fadeInImage.color = new Color(1, 1, 1, alphaOut);
        }

        yield return new WaitForSeconds(3f);
        onFinishFadeIn.Invoke();

        //camTransition?.Invoke();

    }


    IEnumerator FadeIn()
    {
        float alpha = 0f;
        while(alpha < 255f)
        {
            
            yield return null;
            alpha += Time.deltaTime/_fadeSpeed;
            _fadeInImage.color = new Color(1, 1, 1, alpha);
        }  

    }


    IEnumerator FadeOut()
    {
        float alpha = 1f;
        while (alpha > 0)
        {

            yield return null;
            alpha -= Time.deltaTime / _fadeSpeed;
            _fadeInImage.color = new Color(1, 1, 1, alpha);
        }
    }

    private void StartFadeEffect()
    {
        StartCoroutine(FadeEffect());
    }

    private void StartFadeEffectWithNoTransition(int id)
    {
        StartCoroutine(FadeEffectWithNoTransition());
    }

    public void StartFadeTransition()
    {
        StartCoroutine(FadeEffectTransition());
    }

    private void StartFadeIn()
    {
        StartCoroutine(FadeIn());
    }

    private void StartFadeOut()
    {
        StartCoroutine(FadeOut());
    }
}
